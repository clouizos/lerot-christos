#!/bin/sh

TEMPLATE=$HOME/lerot-christos/lisa/lisa/do_kendallClustering.job
TMP_DIR=$HOME

RUNTIME=01
MINUTES=00
RUNDAYS=00
start=`date`
echo $start

JOB_FILE=$TMP_DIR/submit.KendallClustering$1'_'$2.job
cat $TEMPLATE | sed "s/##RUNTIME##/$RUNTIME/" | \
          sed "s/##RUNDAYS##/$RUNDAYS/" | \
          sed "s/##MINUTES##/$MINUTES/" | \
          sed "s/##TYPEDATA##/$1/" | \
	  sed "s/##NUMCLUSTERS##/$2/" > $JOB_FILE

echo "Submitting $JOB_FILE"
qsub -e$JOB_FILE.e -o$JOB_FILE.o $JOB_FILE
sleep 2

