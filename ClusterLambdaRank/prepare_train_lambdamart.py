#!/usr/bin/env python

'''
Create a script file for training (RankLib's) LambdaMART model on every cluster
of queries in the given directory.
'''

import os
import glob
import argparse
import cPickle as pickle

from itertools import groupby, chain
from lerot.query import write_queries


def get_dataset_name(filepath):
    '''
    Return the name of the dataset from its filepath (i.e. directories
    and extensions are removed).
    '''
    filename = os.path.basename(filepath)
    return filename if filename.index('.') < 0 else filename[:filename.index('.')]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
    parser.add_argument('--trees', type=int, default=100, help='specify the number of regression trees (default: %(default)s)')
    parser.add_argument('--leaves', type=int, default=8, help='specify the maximum number of leaves in a tree (default: %(default)s)')
    parser.add_argument('--shrinkage', type=float, default=0.9, help='specify the learning rate')
    parser.add_argument('--vs', type=float, default=0.2, help='specify the fraction of queries reserved for validation (default: %(default)s)')
    parser.add_argument('--gmax', type=int, default=4, help='specify the maximum relevance grade in the dataset (default: %(default)s)')
    parser.add_argument('--metric', type=str, default='NDCG@10', help='specify the IR evaluation measure for training (default: %(default)s)')
    parser.add_argument('--estop', type=int, default=10, help='stop training after not observing improvement on validaton set in this many consecutive rounds (default: %(default)s)')
    parser.add_argument('--prefix', default='query_dataset_cluster_', help="specify the prefix for input dataset filenames (default: '%(default)s')")
    parser.add_argument('--command', default='java -jar RankLib.jar', help="specify the execution command for RankLib (default: '%(default)s')")
    parser.add_argument('--scriptname', help='specify the filename for the script')
    parser.add_argument('--scores', help='specify (optional) output directory for scores assign by the model to each test query')
    parser.add_argument('datasets', help='specify input directory with training and test sets')
    parser.add_argument('models', help='specify output directory for trained models')
    parser.add_argument('evaluations', help='specify output directory for evaluations')

    arguments = parser.parse_args()

    num_trees = arguments.trees
    num_leaves = arguments.leaves
    shrinkage = arguments.shrinkage
    validation_size = arguments.vs
    max_grade = arguments.gmax
    estop = arguments.estop
    metric = arguments.metric
    command = arguments.command
    scriptname = arguments.scriptname

    suffix = 'T%d_L%d_S%.2f_V%.2f_G%d_E%d_M%s' % (num_trees, num_leaves, shrinkage, validation_size, max_grade, estop, metric)

    models_dir = os.path.join(arguments.models, suffix)
    evaluations_dir = os.path.join(arguments.evaluations, suffix)

    if arguments.scores is not None:
        scores_dir = os.path.join(arguments.scores, suffix)
    else:
        scores_dir = None

    # Create the output script filename (if not given).
    if scriptname is None:
        scriptname = 'train_lambdamart_%s.sh' % suffix

    test_dataset_filepaths, train_dataset_filepaths = zip(*zip(*[iter(sorted(glob.glob(os.path.join(arguments.datasets, arguments.prefix + '*'))))]*2))

    # Sanity checks.
    assert len(test_dataset_filepaths) == len(train_dataset_filepaths)

    for i in range(len(train_dataset_filepaths)):
        assert get_dataset_name(train_dataset_filepaths[i]) == get_dataset_name(test_dataset_filepaths[i])

    # Begin writing the script file.
    with open(scriptname, 'w') as scriptfile:
        scriptfile.write('# Create an output directory for the trained models.\n')
        scriptfile.write('mkdir -p %s\n\n' % models_dir)

        scriptfile.write('# Create an output directory for the evaluation of the models.\n')
        scriptfile.write('mkdir -p %s\n\n' % evaluations_dir)

        if scores_dir is not None:
            scriptfile.write('# Create an output directory for the query scores.\n')
            scriptfile.write('mkdir -p %s\n\n' % scores_dir)

        # Fill the script with LamdaMART training commands.
        for i in range(len(train_dataset_filepaths)):
            model_filepath = os.path.join(models_dir, get_dataset_name(train_dataset_filepaths[i]) + '.model.txt')

            # Add command description for easier debugging by eye-balling method :).
            if i == 0:
                scriptfile.write('# Command for training global LambdaMART model (cluster 0).\n')
            else:
                scriptfile.write('# Command for training local LambdaMART model for cluster %d.\n' % i)

            # Write the command for training the LamdaMART model for the current cluster (or the global model, i.e. cluster 0).
            scriptfile.write('%s -silent -ranker 6 -tree %d -leaf %d -shrinkage %.2f -tc -1 -mls 1 -estop %d -gmax %d -tvs %.2f -metric2t %s -train %s -save %s &\n\n' % \
                             (command, num_trees, num_leaves, shrinkage, estop, max_grade, validation_size, metric, train_dataset_filepaths[i], model_filepath))

        # The models need to be trained first before executing their evaluations.
        scriptfile.write('wait\n\n')

        # Fill the script with evaluation commands.
        for i in range(len(test_dataset_filepaths)):
            # The global model is being evaluated on each cluster's test set.
            if i == 0:
                for j in range(1, len(test_dataset_filepaths)):

                    # The global model filepath.
                    model_filepath = os.path.join(models_dir, get_dataset_name(train_dataset_filepaths[i]) + '.model.txt')
                    evaluation_filepath = os.path.join(evaluations_dir, get_dataset_name(test_dataset_filepaths[j]) + '.global.txt')

                    scriptfile.write('# Command for evaluation of the global LambdaMART model on the cluster %d test set.\n' % j)
                    scriptfile.write('%s -load %s -metric2T %s -test %s > %s &\n\n' % (command, model_filepath, metric, test_dataset_filepaths[j], evaluation_filepath))

                    # Should the query scores be also saved.
                    if scores_dir is not None:
                        score_filepath = os.path.join(scores_dir, get_dataset_name(test_dataset_filepaths[j]) + '.global.txt')
                        scriptfile.write('# Command for estimating scores for each query in the cluster %d test set by the global LambdaMART model.\n' % j)
                        scriptfile.write('%s -load %s -metric2T %s -rank %s -score %s &\n\n' % (command, model_filepath, metric, test_dataset_filepaths[j], score_filepath))

            # The local model is evaluated on its associated test set.
            else:
                model_filepath = os.path.join(models_dir, get_dataset_name(test_dataset_filepaths[i]) + '.model.txt')
                evaluation_filepath = os.path.join(evaluations_dir, get_dataset_name(test_dataset_filepaths[i]) + '.local.txt')

                scriptfile.write('# Command for evaluation of the local LambdaMART model for the cluster %d.\n' % i)
                scriptfile.write('%s -load %s -metric2T %s -test %s > %s &\n\n' % (command, model_filepath, metric, test_dataset_filepaths[i], evaluation_filepath))

                # Should the query scores be also saved.
                if scores_dir is not None:
                    score_filepath = os.path.join(scores_dir, get_dataset_name(test_dataset_filepaths[i]) + '.local.txt')
                    scriptfile.write('# Command for estimating scores for each query in the cluster %d test set by the local LambdaMART model.\n' % i)
                    scriptfile.write('%s -load %s -metric2T %s -rank %s -score %s &\n\n' % (command, model_filepath, metric, test_dataset_filepaths[i], score_filepath))

        # Wait till all model evaluations have finished.
        scriptfile.write('wait\n')
