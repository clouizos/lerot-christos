#!/bin/sh

TEMPLATE=$HOME/lerot-christos/lisa/lisa/correlation_plot.job
TMP_DIR=$HOME

RUNTIME=20
MINUTES=00
RUNDAYS=00
start=`date`
echo $start

JOB_FILE=$TMP_DIR/submit.CorrelationPlot$1.job
cat $TEMPLATE | sed "s/##RUNTIME##/$RUNTIME/" | \
          sed "s/##RUNDAYS##/$RUNDAYS/" | \
          sed "s/##MINUTES##/$MINUTES/" | \
          sed "s/##TYPEDATA##/$1/" > $JOB_FILE

echo "Submitting $JOB_FILE"
qsub -e$JOB_FILE.e -o$JOB_FILE.o $JOB_FILE
sleep 2

