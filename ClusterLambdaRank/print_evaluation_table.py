#!/usr/bin/env python

'''
Vizualizes the evaluation data of the trained LambdaMART models into a table.
'''

import os
import glob
import argparse


def get_dataset_name(filepath):
    '''
    Return the name of the dataset from its filepath (i.e. directories
    and extensions are removed).
    '''
    filename = os.path.basename(filepath)
    return filename if filename.index('.') < 0 else filename[:filename.index('.')]


def get_model_description(s):
    '''
    Turn the specified string into the LamdaMART
    model description.
    '''
    description = 'LambdaMART Parameters: %(T)s trees, %(L)s leaves, shrinkage: %(S)s, validation: %(V)s, max. grade: %(G)s, estop: %(E)s, metric: %(M)s.'
    return description % dict((parameter[:1], parameter[1:]) for parameter in s.strip().split('_'))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
    parser.add_argument('evaluations', help='specify the input directory with evaluations')

    arguments = parser.parse_args()

    # Retrieve the LamdaMART model from the directory name.
    model_description = get_model_description(os.path.basename(arguments.evaluations))

    table_datasets = []
    table_globals = []
    table_locals = []
    table_winners = []

    for global_filepath, local_filepath in zip(*[iter(sorted(glob.glob(os.path.join(arguments.evaluations, '*'))))]*2):
        # Sanity check.
        assert get_dataset_name(global_filepath) == get_dataset_name(local_filepath)

        table_datasets.append(get_dataset_name(global_filepath))

        # Read the results for the global model.
        with open(global_filepath) as ifile:
            table_globals.append(ifile.readlines()[-1].split()[-1].strip())

        # Read the results for the local model.
        with open(local_filepath) as ifile:
            table_locals.append(ifile.readlines()[-1].split()[-1].strip())

        no_failure = True

        try:
            global_result = float(table_globals[-1])
        except:
            table_globals[-1] = 'FAILED'
            no_failure = False

        try:
            local_result = float(table_locals[-1])
        except:
            table_locals[-1] = 'FAILED'
            no_failure = False

        if no_failure:
            if global_result > local_result:
                table_winners.append('Global')
            elif global_result < local_result:
                table_winners.append('Local')
            else:
                table_winners.append('Draw')
        else:
            table_winners.append('?')

    # Calculate the table column widths (w/o padding).
    width_col1 = max(max(map(len, table_datasets)), len('Datasets'))
    width_col2 = max(max(map(len, table_globals)), len('Global'))
    width_col3 = max(max(map(len, table_locals)), len('Local'))
    width_col4 = len('Winner')

    table_row_format = '| %%%ds | %%%ds | %%%ds | %%%ds |' % (width_col1, width_col2, width_col3, width_col4)

    # Account for the cell padding.
    width_col1 += 2
    width_col2 += 2
    width_col3 += 2
    width_col4 += 2

    table_line_sep = '+%s+%s+%s+%s+' % ('-' * width_col1, '-' * width_col2, '-' * width_col3, '-' * width_col4)

    # Print the LamdaMART description.
    print model_description

    # Print the table header.
    print table_line_sep
    print table_row_format % ('Dataset', 'Global', 'Local', 'Winner')
    print table_line_sep

    # Print the table content.
    for items in zip(table_datasets, table_globals, table_locals, table_winners):
        print table_row_format % items
        print table_line_sep
