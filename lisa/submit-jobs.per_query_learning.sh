#!/bin/sh

TMP_DIR=$HOME/tmp
#OUT=irj_listwise_perfect
#OUT=irj_listwise_navigational
#OUT=irj_listwise_informational

RUNTIME=10
MINUTES=00



DATA_SET="MSLR" # "2003_hp_dataset" "2003_td_dataset" "2004_hp_dataset"
                       # "2004_np_dataset" "2004_td_dataset" "MQ2007" "MQ2008"
                       # "OHSUMED"
# Y/MS-LR Navigational user_model_args 
P_CLICK="\"0:0.05,1:0.3,2:0.5,3:0.7,4:0.95\""
P_STOP="\"0:0.2,1:0.3,2:0.5,3:0.7,4:0.9\""
FEATURE_COUNT=136 # MQ* - 46; MSLR - 136; Gov - 64 (letor 3);
DATA_DIR="/home/mzoghi/Intent/Data/MSLR/train_per_query"

for i in {1..100}
do
    STAMP=`echo $(date +"%D %T") | sed "s/[^0-9]/_/g"`
    JOB_FILE=$TMP_DIR/submit.$STAMP.job
    echo "Submitting $JOB_FILE"
    cp --parents learning.template.job $JOB_FILE
    qsub -e$JOB_FILE.e -o$JOB_FILE.o $JOB_FILE
    sleep 5
done


# Y/MS-LR Perfect user_model_args
# P_CLICK="\"0:0.0,1:0.2,2:0.4,3:0.8,4:1\""
# P_STOP="\"0:0.0,1:0.0,2:0.0,3:0.0,4:0.0\""
# Y/MS-LR Navigational user_model_args 
# P_CLICK="\"0:0.05,1:0.3,2:0.5,3:0.7,4:0.95\""
# P_STOP="\"0:0.2,1:0.3,2:0.5,3:0.7,4:0.9\""
# Y/MS-LR Informational user_model_args
# P_CLICK="\"0:0.4,1:0.6,2:0.7,3:0.8,4:0.9\""
# P_STOP="\"0:0.1,1:0.2,2:0.3,3:0.4,4:0.5\""
# perfect click model
# P_CLICK="\"0:0.0, 1:0.5, 2:1.0\""
# P_STOP="\"0:0.0, 1:0.0, 2:0.0\""
# navigational
# P_CLICK="\"0:0.05, 1:0.5, 2:0.95\""
# P_STOP="\"0:0.2, 1:0.55, 2:0.9\""
# informational
# P_CLICK="\"0:0.4, 1:0.65, 2:0.9\""
# P_STOP="\"0:0.1, 1:0.3, 2:0.5\""
# perfect click feedback
# P_CLICK="\"0:0.0, 1:1.0\""
# P_STOP="\"0:0.0, 1:0.0\""
# navigational
# P_CLICK="\"0:0.05, 1:0.95\""
# P_STOP="\"0:0.2, 1:0.9\""
# informational
# P_CLICK="\"0:0.4, 1:0.9\""
# P_STOP="\"0:0.1, 1:0.5\""

