import matplotlib
matplotlib.use('Agg')   # since lisa does not have a display we need to pick the appropriate backend for matplotlib
import numpy as np
from scipy.spatial.distance import pdist, squareform
from scipy.cluster.hierarchy import linkage, dendrogram, fcluster
import glob
import pylab
import cPickle as pickle
from weightedKendall import pos_weighted_kendalltau
from lerot.ranker.ProbabilisticRankingFunction import ProbabilisticRankingFunction
from lerot import query
import sys
import time
from multiprocessing import Pool


def parse_rankers(typedata, sample_size='all',
                  np_random_seed=[], to_keep=[]):
    """
    Function that given the type of dataset that you want to load
    parses the corresponding rankers (weight matrix),
    full rankers (list of dictionaries where each dictionary contains more
    information about the ranker), and a list of query ID's corresponding to
    those rankers.

    Parameters
    ----------
    typedata : string
        The type of the dataset that you want to parse.
        Available options are 'MSLR' and 'YLR'.

    sample_size : int or string, optional
        The amount of rankers that you want to sample from the original set.
        If it is an integer it will sample that amount of rankers, if it is a String it
        will parse everything. Defaults to 'all' which parses everything.

    np_random_seed : int, optional
        The seed that is used in order to sample the rankers from the original set.
        Defaults to an empty list.

    to_keep : list of strings, optional
        A list that specifies the rankers that you want to load. Each string in the list
        is the query ID of the ranker. (There is one ranker for each query.) Defaults
        to an empty list

    Output
    -------
    rankers : list of ProbabilisticRankingFunction objects
        A list that contains the instantiated Lerot objects according
        to each of the ranker weights

    weights : numpy array of size (num_rankers, dim_of_dataset)
        A numpy matrix that each row is the weights for each ranker. The rankers are sorted in ascending order.

    full_rankers : list of dictionaries
        A list that contains a dictionary for each ranker where each dictionary contains the following items:
            'path': The original path of the ranker,
            'query': The query of the ranker
            'run': On which 'run' the ranker was created (we created a ranker for a query by picking the best one
                    among a set of trained rankers, each one of them obtained from a separate 'run')
            'system': The system that was used to train the ranker (e.g. Listwise Learning System - 'lws')
            'weights': A numpy array that contains the weights of the ranker
            'rankingfunction' : A ProbabilisticRankingFunction Lerot object for the ranker

    queries : list of strings
        A list that contains the query ID's for the rankers that were parsed.
    """
    # Extract the paths of the rankers sorting them according to the qid of
    # the corresponding queries
    if typedata == 'MSLR':
        ranker_paths = sorted(glob.glob('/home/mzoghi/lerot-christos/Rankers_pq_all_MSLR/Group*/BestRankers/b*'), key=lambda x: int(x.split('_')[-1].strip('.pkl')))
    elif typedata == 'YLR':
        ranker_paths = sorted(glob.glob('/home/mzoghi/lerot-christos/rankers_pq_all_YLR/Group*/BestRankers/b*'), key=lambda x: int(x.split('_')[-1].strip('.pkl')))

    # initialize each one of the lists
    rankers = []
    weights = []
    full_rankers = []

    # if we want to keep specific rankers keep only the relevant parts
    # and discard the rest
    if to_keep:
        ranker_paths = [ranker for ranker in ranker_paths if ranker.split('_')[-1].strip('.pkl') in to_keep]

    # Gather a random subset of the ranker paths if need be
    elif sample_size != 'all':
        if np_random_seed != []:
            # MZ: I added this to be able to control which rankers are chosen
            #     across different jobs.
            np.random.seed(seed=np_random_seed)
        # if we have a correct sample size
        assert isinstance(sample_size, (int, long)) and sample_size <= len(ranker_paths)
        sample_rankers = np.random.permutation(len(ranker_paths))[0:sample_size]
        ranker_paths = [ranker_paths[i] for i in sample_rankers]
        T = time.time()
        T = int((T - int(T)) * 10 ** 9)
        np.random.seed(seed=T)

    # Load the rankers here
    for ranker_path in ranker_paths:
        with open(ranker_path, 'rb') as handle:
            tmp_r = pickle.load(handle)
            tmp_w = tmp_r['weights']
            weights.append(tmp_w)

            if typedata == 'MSLR':
                ranker = ProbabilisticRankingFunction('3', 'random', 136,
                               str(tmp_w.tolist()).strip('[]'), sample='sample_unit_sphere')
            elif typedata == 'YLR':
                ranker = ProbabilisticRankingFunction('3', 'random', 700,
                               str(tmp_w.tolist()).strip('[]'), sample='sample_unit_sphere')

            tmp_r['rankingfunction'] = ranker
            full_rankers.append(tmp_r)
            rankers.append(ranker)

    # Return the requested items
    return rankers, np.array(weights), full_rankers, [full_rankers[i]['query'] for i in xrange(len(full_rankers))]


def parse_queries(typedata, sampled_q, clusters=None):
    """
    Function that returns the query-document feature vectors.

    Parameters
    -----------
    typedata : string
        Specify the type of the dataset that you want to load.
        Available are 'MSLR' and 'YLR'

    sampled_q : list (can be empty)
        Specify which query ID's you want to keep and return the
        query document pairs only for those

    clusters : list of lists of strings, optional
        Each sublist of the original list is a cluster
        and each string in the sublist is a query ID.
        Default value is None

    Output
    -------
    new_queries : Lerot Query object or a list of Lerot Query objects
        A Lerot query object in the case when a 'clusters' list is not passed
        If a 'clusters' is passed then this function returns a list of
        len(clusters) + 1 size that contains the data for each one of the clusters
        and the the data of all of the clusters combined at the final position
    """
    query_ids = '/home/mzoghi/Intent/Data/'+typedata+'/all_queries.txt'
    print 'loading all queries first...'
    if typedata == 'MSLR':
        qs = query.load_queries(query_ids, 136)
    elif typedata == 'YLR':
        qs = query.load_queries(query_ids, 700)

    if not sampled_q and clusters is None:
        print 'Did not specify which queries to keep. Returning all of them...'
        return qs

    print 'Done. Keeping specific ones..'
    # workaround for keeping only the queries corresponding
    # to the specific rankers
    if clusters is not None:
        new_queries = []
        all_q = {}
        for cluster in clusters:
            tmp = {}
            for q in cluster:
                tmp[q] = qs[q]
                all_q[q] = qs[q]
            new_queries.append(query.Queries(tmp, 136, read_q=False))
        # final one are all the queries in one for the global model
        new_queries.append(query.Queries(all_q, 136, read_q=False))
    else:
        new_q = {}
        for id_q in sampled_q:
            new_q[id_q] = qs[id_q]
        if typedata == 'MSLR':
            new_queries = query.Queries(new_q, 136, read_q=False)
        elif typedata == 'YLR':
            new_queries = query.Queries(new_q, 700, read_q=False)
    print 'done.'
    return new_queries


def estimate_kendall_dist_mat(rankers1, rankers2, queries1, queries2, cutoff=10):
    """
    Function that estimates the Kendall distance matrix between two given sets of rankers

    Parameters
    -----------
    rankers1 : list of dictionaries
        Each dictionary in the list corresponds to a ranker and the keys that need to
        be in the dictionary are:
            'query' : The query ID of the ranker
            'rankingfunction' : A ProbabilisticRankingFunction lerot object for the ranker

    rankers2 : list of dictionaries
        Same as the rankers1 parameter

    queries1 : Lerot Query Object
        It contains the query-document feature vectors for the rankers1 set of rankers

    queries2 : Lerot Query Object
        Same as queries1

    cutoff : int, optional
        The cutoff for the ranking list, default value is 10.
        E.g. for a cutoff of 10 the generalized kendall's distance will
        be estimated only according to the top 10 elements of the ranking list

    Output
    -------
    distance_matrix : numpy array of size (len(rankers1), len(rankers2))
        The dissimilarity matrix for the rankers according to the generalized kendall's distance
    """
    distance_matrix = np.zeros([len(rankers1), len(rankers2)])

    for i in xrange(len(rankers1)):
        for j in xrange(len(rankers2)):
            # same query continue
            if rankers1[i]['query'] == rankers2[j]['query']:
                continue

            # the queries that the pair of rankers will be tested
            qs = [queries1[rankers1[i]['query']],
                  queries2[rankers2[j]['query']]]
            ds = []
            # measure kendall distance between the two specialized rankers
            # only at their specific queries
            # keep the maximum value as the final dissimilarity for the rankers
            for q in qs:
                rankers1[i]['rankingfunction'].init_ranking(q)
                rankers2[j]['rankingfunction'].init_ranking(q)
                ranks_1 = [doc.get_id() for doc in rankers1[i]['rankingfunction'].get_ranking()[0:cutoff]]
                ranks_2 = [doc.get_id() for doc in rankers2[j]['rankingfunction'].get_ranking()[0:cutoff]]
                tau, _ = pos_weighted_kendalltau(ranks_1, ranks_2)
                ds.append(tau)
            distance_matrix[i, j] = max(ds)

    return distance_matrix


def cluster_rankers_helper(args):
    """ Helper function for the multiprocessing
        Parse the required arguments and start the clustering"""
    weight_matrix, typedata, precomputed, num_clusters, query_keys = args
    qcl, numcl = cluster_rankers(weight_matrix, typedata, precomputed=precomputed, num_clusters=num_clusters, query_keys=query_keys)
    return qcl, numcl


def cluster_rankers(weight_matrix, typedata, precomputed=False, num_clusters=4, query_keys=None):
    """
    Performs hierarchical clustering with complete linkage on a weight matrix.

    Parameters
    -----------
    weight_matrix : numpy array of size (num_rankers, dim_of_ranker) or (num_rankers, num_rankers)
        The weight matrix to be clustered.

    typedata : string
        The dataset that will be clustered. Available options are 'MSLR' and 'YLR'

    precomputed : Boolean, optional
        If precomputed is True then the weight matrix is assumed to already be a
        dissimilarity matrix of size (num_rankers, num_rankers). Otherwise it is
        interpreted as a weight matrix of size (num_rankers, dim_of_ranker) and a
        dissimilarity matrix is created using Euclidean distance between the rows.
        Defaults to False

    num_clusters : int, optional
        The number of clusters that we want to create from the tree. Defaults to 4.

    query_keys : list of strings, optional
        The list of query IDs (sorted in ascending order) that will be clustered.

    Output
    -------
    q_clusters : dictionary
        Dictionary of the clustering. Keys are the query ID's and their
        corresponding values are the cluster that they are assigned to.
        Contains an extra key 'numofclusters' that its value is the
        total number of clusters

    num_clusters : int
        The number of clusters

    It will also save an image of the resulting clustering as a heatmap under the name
    'HierarchicalClustering_'+typedata+'_'+str(num_clusters)+'.png' on the directory
    that it is called.
    """
    fig = pylab.figure(figsize=(10, 10))
    if precomputed:
        print_now('Distance matrix already precomputed')
        mat = weight_matrix
    else:
        sc = pdist(weight_matrix, 'euclidean')  # 'minkowski', 1)
        mat = squareform(sc)

    ax1 = fig.add_axes([0.09, 0.1, 0.2, 0.6])
    print_now('Doing once...')
    Y = linkage(mat, method='complete')
    print_now('Done. Doing dendrogram...')
    Z = dendrogram(Y, orientation='right')
    ax1.set_xticks([])
    ax1.set_yticks([])
    print_now('Done.')
    ax2 = fig.add_axes([0.3, 0.71, 0.6, 0.2])
    print_now('Doing twice...')
    Y2 = linkage(mat, method='complete')
    print_now('Done. Doing dendrogram...')
    Z2 = dendrogram(Y2, orientation='top')
    ax2.set_xticks([])
    ax2.set_yticks([])
    print_now('Done. Plotting and saving the image...')
    idx1 = Z['leaves']
    idx2 = Z2['leaves']
    mat = mat[idx1, :]
    mat = mat[:, idx2]
    axmatrix = fig.add_axes([0.3, 0.1, 0.6, 0.6])
    min_to_keep = 0
    for elem in np.sort(mat.ravel()):
        if elem > min_to_keep:
            min_to_keep = elem
            break

    im = axmatrix.matshow(mat, origin='lower', aspect='auto', vmin=min_to_keep)
    axmatrix.set_xticks([])
    axmatrix.set_yticks([])

    axcolor = fig.add_axes([0.91, 0.1, 0.02, 0.6])
    pylab.colorbar(im, cax=axcolor)
    #fig.show()
    fig.savefig('HierarchicalClustering_'+typedata+'_'+str(num_clusters)+'.png')
    #plt.show()
    print_now('Done.')
    q_clusters = {}
    if query_keys is not None:
        clustering = fcluster(Y, num_clusters, 'maxclust')
        q_clusters['numofclusters'] = num_clusters
        if query_keys is not None:
            for i, q_key in enumerate(query_keys):
                q_clusters[q_key] = clustering[i]

    return q_clusters, num_clusters


def print_now(string):
    """
    Useful for when we debug jobs on lisa
    Flush the stdout to see the output of the running job immediately
    """
    print string
    sys.stdout.flush()


def mainClustering():
    """
    Function that demonstrates the pipeline
    """
    # the type of the data is parsed from the command line
    typedata = sys.argv[1]
    print_now('Working on ' + typedata + '...')
    print_now('Loading the rankers...')
    rankers, weights, full_rankers, queries_to_parse = parse_rankers(typedata, sample_size=300)

    # optional saving of the stuff that we parse
    #with open('weights_rankers_'+typedata+'.pkl', 'wb') as handle:
    #    pickle.dump(weights, handle)
    #with open('rankers_'+typedata+'.pkl', 'wb') as handle:
    #    pickle.dump(rankers, handle)
    #with open('full_rankers_'+typedata+'.pkl', 'wb') as handle:
    #    pickle.dump(full_rankers, handle)
    # optional loading of previous stuff
    #with open('rankers_'+typedata+'.pkl', 'rb') as handle:
    #     rankers = pickle.load(handle)

    print_now('Loading the queries...')
    queries = parse_queries(typedata, queries_to_parse)

    # again optional saving and loading
    #with open('sampled_queries_'+typedata+'.pkl', 'wb') as handle:
    #    pickle.dump(queries, handle)
    #with open('sampled_queries_'+typedata+'.pkl', 'rb') as handle:
    #     queries = pickle.load(handle)

    print_now('Estimating the distance matrix...')
    distance_matrix = estimate_kendall_dist_mat(rankers, rankers, queries, queries, cutoff=10)
    # optional saving and loading
    with open('dist_mat_'+typedata+'.pkl', 'wb') as handle:
       pickle.dump(distance_matrix, handle)
    #with open('dist_mat_'+typedata+'.pkl', 'rb') as handle:
    #    distance_matrix = pickle.load(handle)


def mainClusteringLisa():
    """
    Function that loads a precomputed distance matrix and performs parallel clusterings
    with a different number of clusters

    (Needs to be executed in lisa so as to get the correct paths)
    """
    # the type of the data is parsed from the command line
    typedata = sys.argv[1]
    print_now('Working on ' + typedata + '...')

    # num of clusters that we will try
    totry = [15, 25, 35, 45, 50]
    # create a pool of processes for each clustering
    pool = Pool(len(totry))

    # load the precomputed stuff
    print_now('Loading distance matrix...')
    distance_matrix = np.load('/home/mzoghi/Intent/Output_Dist_Mtx/dm_MSLR_filled.npz')['distanceMat']
    print_now('Loading query ids...')
    q_ids = np.load('/home/mzoghi/Intent/Output_Dist_Mtx/query_order.npz')['queryIDs'].tolist()

    print_now('Performing hierarchical clustering...')
    # perform multiple clusterings in parallel
    args = zip([distance_matrix]*len(totry), [typedata]*len(totry), [True]*len(totry), totry, [q_ids]*len(totry))
    for q_clusters, numcl in pool.map(cluster_rankers_helper, args):
        with open('qclusters_'+typedata+'_'+str(numcl)+'.pkl', 'wb') as handle:
            pickle.dump(q_clusters, handle)

    # serial
    # for numcl in totry:
    #     # q_clusters = cluster_rankers(distance_matrix, typedata, precomputed=True, num_clusters=int(sys.argv[2]), query_keys=q_ids)
    #     q_clusters = cluster_rankers(distance_matrix, typedata, precomputed=True, num_clusters=numcl, query_keys=q_ids)
    #     with open('qclusters_'+typedata+'_'+str(numcl)+'.pkl', 'wb') as handle:
    #         pickle.dump(q_clusters, handle)

    print_now('Done.')

if __name__ == '__main__':
    # mainClustering()
    mainClusteringLisa()
