#!/bin/sh

TEMPLATE="$HOME/online-learning-code/src/lisa/evaluation.template.job"
TMP_DIR="$HOME/tmp"

CONFIG_FILE="evaluation.config.yml"

RUNTIME=50
MINUTES=00

for CLICK_MODEL in "realistic" "high-noise" "perfect" "high-position-bias" # "perfect" "ndcg-perfect" "realistic" "high-noise" "high-position-bias" "navigational" "informational" "almost-random"
do
    OUT=all-oct-19-$CLICK_MODEL
    echo $CLICK_MODEL
    # syntax for seq: FIRST INCREMENT LAST
    # 0..992 will give 63 * 16 = 1008 runs
    # 0..496 will give 31 * 16 = 512 runs
    for RUN_START_ID in `seq 0 16 992`; do
        if   [ "$CLICK_MODEL" = "perfect" ]; then
            # perfect:
            #user_model_args: "--p_click 0:0.0, 1:0.2, 2: 0.4, 3: 0.8, 4:1.0 --p_stop 0:.0, 1:.0, 2:.0, 3:.0, 4:.0"
            P_CLICK="\"0:0.0, 1:0.2, 2: 0.4, 3: 0.8, 4:1.0\""
            P_STOP="\"0:.0, 1:.0, 2:.0, 3:.0, 4:.0\""
        elif [ "$CLICK_MODEL" = "ndcg-perfect" ]; then
            P_CLICK="\"0:0.0, 1:0.07, 2:0.2, 3:0.47, 4:1.0\""
            P_STOP="\"0:.0, 1:.0, 2:.0, 3:.0, 4:.0\""
        elif [ "$CLICK_MODEL" = "realistic" ]; then
            P_CLICK="\"0:0.49, 1:0.45, 2:0.55, 3:0.71, 4:0.94\""
            P_STOP="\"0:.43, 1:.40, 2:.49, 3:.67, 4:.94\""
        elif [ "$CLICK_MODEL" = "high-noise" ]; then
            P_CLICK="\"0:0.3, 1:0.4, 2:0.5, 3:0.6, 4:0.7\""
            P_STOP="\"0:.0, 1:.0, 2:.0, 3:.0, 4:.0\""
        elif [ "$CLICK_MODEL" = "high-position-bias" ]; then
            P_CLICK="\"0:0.49, 1:0.45, 2:0.55, 3:0.71, 4:0.94\""
            P_STOP="\"0:.7, 1:.7, 2:.7, 3:.7, 4:.7\""
        elif [ "$CLICK_MODEL" = "navigational" ]; then
            # navigational:
            #user_model_args: "--p_click 0:0.05, 1:0.1, 2: 0.2, 3: 0.4, 4:0.8 --p_stop 0:.0, 1:.2, 2:.4, 3:.6, 4:.8"
            P_CLICK="\"0:0.05, 1:0.1, 2: 0.2, 3: 0.4, 4:0.8\""
            P_STOP="\"0:.0, 1:.2, 2:.4, 3:.6, 4:.8\""
        elif [ "$CLICK_MODEL" = "informational" ]; then
            # informational:
            #user_model_args: "--p_click 0:0.4, 1:0.6, 2: 0.7, 3: 0.8, 4:0.9 --p_stop 0:.1, 1:.2, 2:.3, 3:.4, 4:.5"
            P_CLICK="\"0:0.4, 1:0.6, 2: 0.7, 3: 0.8, 4:0.9\""
            P_STOP="\"0:.1, 1:.2, 2:.3, 3:.4, 4:.5\""
        elif [ "$CLICK_MODEL" = "almost-random" ]; then
            # almost-random:
            #user_model_args: "--p_click 0:0.4, 1:0.45, 2: 0.5, 3: 0.55, 4:0.6 --p_stop 0:.5, 1:.5, 2:.5, 3:.5, 4:.5"
            P_CLICK="\"0:0.4, 1:0.45, 2: 0.5, 3: 0.55, 4:0.6\""
            P_STOP="\"0:.5, 1:.5, 2:.5, 3:.5, 4:.5\""
        else
            echo "Unknown click model $CLICK_MODEL!"
            exit -1
        fi

        JOB_FILE=$TMP_DIR/submit.$RANDOM.job
        cat $TEMPLATE | sed "s/##OUT##/$OUT/" | \
            sed "s/##CONFIG_FILE##/$CONFIG_FILE/" | \
            sed "s/##P_CLICK##/$P_CLICK/" | \
            sed "s/##P_STOP##/$P_STOP/" | \
            sed "s/##RUN_START_ID##/$RUN_START_ID/" | \
            sed "s/##RUNTIME##/$RUNTIME/" | \
            sed "s/##MINUTES##/$MINUTES/" > $JOB_FILE
        echo "Submitting $JOB_FILE"
        qsub -e$JOB_FILE.e -o$JOB_FILE.o $JOB_FILE
        sleep 1
    done
done
