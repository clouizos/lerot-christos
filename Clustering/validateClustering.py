import cPickle as pkl
import sys
import random
from lerot import retrieval_system, environment, evaluation, comparison
from multiprocessing import Pool
from math import ceil
sys.path.append('/home/mzoghi/lerot-christos/Clustering')  # add the location of the clustering_experiments script
from clustering_experiments import parse_queries, print_now


def loadClustering(location):
	"""
	Load the query clusters pickle

	Parameters
	-----------
	location : string
		Location of the pickle on the disk

	Output
	-------
	qclusters : dictionary
		A dictionary where the keys are the query ID's and the
		values are the cluster assignment of the query
	"""
	with open(location, 'rb') as handle:
		qclusters = pkl.load(handle)
	return qclusters


def doAssignments(qclusters):
	"""
	Function that generates a list of lists that corresponds to a clustering

	Parameters
	-----------
	qclusters : dictionary
		A dictionary where the keys are the query ID's and the
		values are the cluster assignment of the query

	Output
	-------
	clusters : list of lists of strings
		Each sublist corresponds to a cluster and it contains
		the query ID's for that cluster.
	"""
	clusters = [[] for i in xrange(qclusters['numofclusters'])]
	for key in qclusters:
		if key != 'numofclusters':
			clusters[qclusters[key] - 1].append(key)
	return clusters


def retrieveData(clusters):
	"""
	Function that returns the query-document feature vectors
	for each one of the clusters

	Parameters
	-----------
	clusters : list of lists of strings
		Each sublist corresponds to a cluster and it contains
		the query ID's for that cluster.

	Output
	-------
	qdata : list of Lerot query objects
		Each position in the list corresponds to the Lerot query object
		that contains the query-document feature vectors for each cluster.
		The final element contains a query object that corresponds to the
		data from all of the clusters, which is used for training the
		global model.
	"""
	# also returns all the data combined (the global model)
	qdata = parse_queries('MSLR', [], clusters=clusters)
	return qdata


def helper_learn(args):
	"""
	Helper function for the multiprocessing, unrolls the arguments
	"""
	data, modeltype, numclusters = args
	learner, testing = learn(data, modeltype, numclusters)
	return learner, testing


def learn(data, modeltype, numclusters):
	"""
	Function that trains a ranker on the query-document feature vectors of a cluster

	Parameters
	-----------
	data : Lerot Query object
		Contains the query-document feature vectors for a cluster
		80%  of it will be used for training and 20%  for testing

	modeltype : string
		The type of the model that you want to train. Available options are:
			'local' : a model that is trained for 1000 iterations
			'global' : a model that is trained for 1000*numclusters iterations
						(it is unfair to the global model to have the same number
						 of iterations as the smaller local models)

	numclusters : int
		The number of clusters in the clustering (Only used in the training of the global ranker)

	Output
	-------
	learner : Lerot ListwiseLearningSystem object
		The trained system on the query-document feature vectors of the cluster

	testing : list of strings
		A list that contains the query ID's that will be used for testing
	"""
	if modeltype == 'local':
		train_iter = 10000
	# unfair to the global model to have the same number of iterations
	# increase them according to the number of clusters
	elif modeltype == 'global':
		train_iter = 10000 * numclusters

	# initialize the retrieval system and the user model
	learner = retrieval_system.ListwiseLearningSystem(136, '-w random -c comparison.ProbabilisticInterleave -r ranker.ProbabilisticRankingFunction -s 3 -d 0.1 -a 0.01')
	user_model = environment.CascadeUserModel('--p_click 0:0.05,1:0.3,2:0.5,3:0.7,4:0.95 --p_stop 0:0.2,1:0.3,2:0.5,3:0.7,4:0.9')
	
	# singleton clusters
	if len(data.keys()) == 1:
		return learner, []
	
	# assing queries to train and test set
	qtest = int(ceil(0.2*len(data)))
	training, testing = [], []
	qkeys = data.keys()
	print_now('lendata: '+str(len(data))+' qtest: '+str(qtest))
	for i in xrange(len(data)):
		if len(data) - i > qtest:
			training.append(qkeys[i])
		else:
			testing.append(qkeys[i])
	print_now('training: '+str(len(training)))
	print_now('testing: '+str(len(testing)))

	# start training the specialized ranker
	# sample a query from the training set
	# and update the weights according to
	# clicks from the user model
	for i in xrange(train_iter):
		print_now('Iteration '+str(i+1))
		q = random.choice(training)
		print_now('sampled')
		l = learner.get_ranked_list(data[q])
		print_now('ranked')
		c = user_model.get_clicks(l, data[q].get_labels())
		print_now('clicks')
		learner.update_solution(c)
		print_now('updated')

	return learner, testing


def trainPerCl(qdata, p):
	"""
	Function that does the parallel training of the cluster-specialized rankers

	Parameters
	-----------
	qdata : List of Lerot query objects
		Each element in the list corresponds the per cluster training data

	Output
	-------
	trained_systems : list of tuples
		Each element in the list corresponds to a cluster and
		each tuple contains the trained ListwiseLearningSystem and the
		testing query ID's for a cluster

	This function also saves the trained_systems as a pickle at the
	directory from where it was called.
	"""
	# initialize the pool and the list that will be returned
	trained_systems = []
	# construct the arguments for each process
	typemodel = ['local'] * (len(qdata)-1) + ['global']
	numcl = [len(qdata)-1] * len(qdata)
	# single process
	for args in zip(qdata, typemodel, numcl):
		learner, testing = helper_learn(args)
		trained_systems.append((learner, testing))
	# multi process
	#for learner, testing in p.map(helper_learn, zip(qdata, typemodel, numcl)):
	#	trained_systems.append((learner, testing))
	# close the pool to avoid memory allocation errors
	p.close()
	p.join()

	# save the trained systems on the present directory
	# and return them
	with open('perClusterTrainingMSLR_'+str(numcl[0])+'.pkl', 'wb') as handle:
		pkl.dump(trained_systems, handle)

	return trained_systems


def testPerCl(trained_systems, data):
	"""
	Function that performs offline (NDCG) and online (win rates) evaluation between the
	cluster-specific rankers and the global ranker.

	Parameters
	-----------
	trained_systems : list of tuples
		Each element in the list corresponds to a cluster and
		each tuple contains the trained ListwiseLearningSystem and the
		testing query ID's for a cluster. The final position corresponds
		to the global model.

	data : list of Lerot query objects
		Each position in the list corresponds to the Lerot query object
		that contains the query-document feature vectors for each cluster.
		The final element contains a query object that corresponds to the
		data from all of the clusters.

	Output
	-------
	win_rate : list of lists of
		Each position in the list contains a list that has the win rate
		for the local model on position 0 and the win rate for the global
		model on position 1. The win rate is determined according to the
		local model's testing queries.

	ndcg_diff : list of lists
		Each position in the list contains a list that has the NDCG@10
		for the local model on position 0 and the NDCG@10 for the global
		model on position 1. The NDCG@10 is determined according to the
		local model's testing queries.
	"""
	eval_iter = 5000  # online evaluation iterations
	# initialize the user model (same that was used for training)
	# the offline evaluation metric and the interleaving method
	user_model = environment.CascadeUserModel('--p_click 0:0.05,1:0.3,2:0.5,3:0.7,4:0.95 --p_stop 0:0.2,1:0.3,2:0.5,3:0.7,4:0.9')
	evalu = evaluation.NdcgEval()
	comp = comparison.ProbabilisticInterleave()
	# get the global model
	global_model = trained_systems[-1][0]

	# we will use the top 10 documents
	num_docs, win_rate, ndcg_diff = 10, [], []
	for i, (local_model, testing) in enumerate(trained_systems[:-1]):
		print_now('Doing '+str(i+1)+' and global')
		# gather the local testing data
		local_data = data[i]
		# first is local, second is global
		win_rate.append([0, 0])
		ndcg_diff.append([0, 0])
		print_now('testing: '+str(len(testing)))

		if len(testing) == 0:
			print 'Empty test set. Continuing to the next cluster...'
			continue

		print_now('NDCG eval')
		# for each query
		for q in local_data.keys():
			local_q = local_data[q]
			local_list = local_model.get_ranked_list(local_q)
			global_list = global_model.get_ranked_list(local_q)
			local_ndcg = evalu.get_value(local_list,
											local_q.get_labels().tolist(),
											None, cutoff=10)
			global_ndcg = evalu.get_value(global_list,
											local_q.get_labels().tolist(),
											None, cutoff=10)
			ndcg_diff[i][0] += local_ndcg
			ndcg_diff[i][1] += global_ndcg
		# normalize the ndcg scores
		ndcg_diff[i][0] /= len(local_data.keys())
		ndcg_diff[i][1] /= len(local_data.keys())

		print_now('Online eval')
		for j in xrange(eval_iter):
			# pick a random query from the test set
			test_q = local_data[random.choice(testing)]
			# do interleaved comparisons and assign credit accordingly
			local_w = local_model.ranker
			global_w = global_model.ranker
			(interleaved_list, assignments) = comp.interleave(local_w, global_w, test_q, num_docs)
			c = user_model.get_clicks(interleaved_list, test_q.get_labels())
			outcome = comp.infer_outcome(interleaved_list, assignments, c, test_q)
			if outcome < 0:
				win_rate[i][0] += 1
			elif outcome > 0:
				win_rate[i][1] += 1
			else:
				win_rate[i][0] += .5
				win_rate[i][1] += .5

	# normalize
	for win_r in win_rate:
		win_r[0] /= eval_iter
		win_r[1] /= eval_iter

	return win_rate, ndcg_diff


def main():
	# get the number of clusters from the command line
	numcl = sys.argv[1]
	p = Pool()
	print_now('Testing with '+str(numcl)+' clusters')
	location = '/home/mzoghi/lerot-christos/Clustering/qclusters_MSLR_'+numcl+'.pkl'
	#print_now('Loading clusters')
	#qclusters = loadClustering(location)

	#print_now('Doing assignments')
	#clusters = doAssignments(qclusters)

	#print_now('Retrieving data')
	#qdata = retrieveData(clusters)
	# optional saving and loading of qdata
	# with open('qdata_'+numcl+'.pkl', 'wb') as handle:
	# 	pkl.dump(qdata, handle)
	with open('qdata_'+numcl+'.pkl', 'rb') as handle:
	 	qdata = pkl.load(handle)

	print_now('Training systems')
	trained_systems = trainPerCl(qdata, p)
	# optional saving and loading of the trained systems and their
	# correspond testing queries IDs
	# with open('perClusterTrainingMSLR_'+numcl+'.pkl', 'rb') as handle:
	# 	trained_systems = pkl.load(handle)
	# with open('perClusterTrainingMSLR_'+numcl+'.pkl', 'wb') as handle:
	# 	pkl.dump(trained_systems, handle)

	print_now('Done')
	print_now('Testing systems')
	win_rate, ndcg_diff = testPerCl(trained_systems, qdata)

	# save the win rates and ndcg
	with open('perClusterWinRate_'+numcl+'.pkl', 'wb') as handle:
		pkl.dump(win_rate, handle)
	with open('perClusterNDCG_'+numcl+'.pkl', 'wb') as handle:
		pkl.dump(ndcg_diff, handle)

	print_now('Done')


if __name__ == "__main__":
	main()
