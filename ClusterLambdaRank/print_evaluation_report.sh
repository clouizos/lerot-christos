#!/bin/bash

EVALUATIONS_DIR=evaluations

echo "Saving evalutions to: evaluation_report.txt:"
for model_results in `ls ${EVALUATIONS_DIR}`
do
    echo "Generating report for ${model_results}."
    ./print_evaluation_table.py ${EVALUATIONS_DIR}/${model_results} >> evaluation_report.txt
    echo >> evaluation_report.txt
done
