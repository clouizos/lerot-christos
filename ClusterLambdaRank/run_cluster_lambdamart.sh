#!/bin/bash

DATASET_DIR=datasets
MODELS_DIR=models
EVALUATIONS_DIR=evaluations

echo -n "Preparing the datasets..."
# Prepare the cluster datasets (+ global dataset) for training local LamdaMART  models (global).
./prepare_clustered_datasets.py --seed 42 clusters/sampled_queries_MSLR.pkl clusters/qclusters_MSLR_wo1.pkl ${DATASET_DIR}
echo " done."

for ntrees in 50 100
do
    for nleaves in 4 7
    do
        for metric in NDCG
        do
            for atK in 5 10
            do
                # This will generate a script called train_lambdamart.sh which is subsequently executed.
                ./prepare_train_lambdamart.py --trees ${ntrees} --leaves ${nleaves} --shrinkage 0.9 --vs 0.2 --gmax 4 --metric ${metric}@${atK} --estop 10 --scores scores --scriptname train_lambdamart.sh ${DATASET_DIR} ${MODELS_DIR} ${EVALUATIONS_DIR}

                echo -n "Training LamdaMART models: TREES: ${ntrees}, LEAVES: ${nleaves}, METRIC: ${metric}@${atK}..."
                bash train_lambdamart.sh &> /dev/null
                rm train_lambdamart.sh
                echo " done."
            done
        done
    done
done

echo "Saving evalutions to: evaluation_report.txt:"
for model_results in `ls ${EVALUATIONS_DIR}`
do
    echo "Generating report for ${model_results}."
    ./print_evaluation_table.py ${EVALUATIONS_DIR}/${model_results} >> evaluation_report.txt
    echo >> evaluation_report.txt
done

# Remove evaluations -- they are only good for producing the evaluation report
rm -rf ${EVALUATIONS_DIR}
