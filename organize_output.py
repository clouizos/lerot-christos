import glob
import shutil
import os
import sys

"""Script that organizes the rankers according to the query groups that were created
    beforehand (from the split_q_group.py). Takes as an argument the dataset that is used.
    It needs to be executed from inside /home/mzoghi/lerot-christos in order for
    the relative paths to work correctly."""

# parse all the generated config files
directory = '../Intent/Data/'+sys.argv[1]+'/'
directory_out = '../Intent/Output_q_all_'+sys.argv[1]+'/'

# parse all the groups that were created
groups = glob.glob(directory+'Config_pq_all/G*')
group_file_names = {}
# parse all the rankers
print 'Parsing all rankers...'
if sys.argv[1] == 'YLR':
    out_rankers = glob.glob(directory_out+'q*')
else:
    out_rankers = glob.glob(directory_out+'t*')
print 'Rankers parsed.', 'Total number:', len(out_rankers)

# for each group parse the config files for the queries
for group in groups:
    group_file_names[group] = glob.glob(group+'/c*')

print 'Moving each output to its respective query group...'
for i, key in enumerate(group_file_names):
    print 'Group', i+1, 'out of', len(group_file_names)
    # isolate the group ID from the path
    group_id = key.split('/')[-1]
    # make the directory if it doesnt exist
    new_dir = directory_out+group_id
    try:
        os.makedirs(new_dir)
    except:
        print 'Directory already exists! Skipping creation...'
        pass

    for elem in group_file_names[key]:
        # isolate the query ID from the path
        q_id = elem.split('/')[-1].split('_')[-2]
        # parse all the rankers for this query
        rankers_for_q = [ranker for ranker in out_rankers if '_'+q_id+'_' in ranker]
        for rank in rankers_for_q:
            # move each ranker for a query to its respective group
            shutil.move(rank, new_dir)

print 'Finished.'
