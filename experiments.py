from __future__ import division
import cPickle as pickle
import numpy as np
import glob
from scipy.spatial.distance import pdist, squareform
from lerot.ranker.ProbabilisticRankingFunction import ProbabilisticRankingFunction
from lerot import query
import random
from scipy.stats import kendalltau
from lerot.environment.CascadeUserModel import CascadeUserModel
from lerot.evaluation.NdcgEval import NdcgEval
from lerot.comparison.ProbabilisticInterleave import ProbabilisticInterleave
from itertools import combinations
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sys
import gzip
import yaml


def create_correlation_plots_Eucl_Kend_NDCG_Preference(typedata):
    """
    Function that measures and plots correlation between four metrics
    for rankers:
        Euclidean distance between their weights
        Kendall's tau between their ranking lists
        NDCG difference between their rankings
        Win rate of rankers determined via pairwise comparisons

    Parameters
    -----------
    typedata : string
        Denotes the dataset that will be used. Available options are: 'MSLR' and 'YLR'.

    Output
    -------
    Saves the correlation plots in the directory that the script is called under the
    following names:
        'eucl-ken-'+typedata+'.png'
            for correlation plot between euclidean distance and kendall's tau
        'eucl-ndcg-'+typedata+'.png'
            for correlation plot between euclidean distance and the NDCG
            difference between the rankers
        'ken-ndcg-'+typedata+'.png'
            for correlation plot between kendall's tau and NDCG difference
            between the rankers
        'pair-ndcg-'+typedata+'.png'
            for correlation plot between pairwise preferences and NDCG
            difference between the rankers
        'pair-kendall-'+typedata+'.png'
            for correlation plot between pairwise preferences and kendall's tau

    Furthermore it also saves the measurements for the plots in an .npz file under
    the name 'CorrelationData' in the directory that the script is called.
    """
    rankers = []
    # get the ranker paths that we will load according to the dataset
    if typedata == 'MSLR':
        tot_to_read = glob.glob('/home/mzoghi/lerot-christos/Rankers_pq_all_MSLR/Group*/BestRankers/b*')
    elif typedata == 'YLR':
        tot_to_read = glob.glob('/home/mzoghi/lerot-christos/rankers_pq_all_YLR/Group*/BestRankers/b*')

    # parse all the rankers
    for i, ranker in enumerate(tot_to_read):
        print i + 1, 'out of', len(tot_to_read)
        with open(ranker, 'rb') as handle:
            rankers.append(pickle.load(handle))

    print 'Parsed', len(rankers), 'rankers.'
    rankers_qid, rankers_w = [], []
    # add the query ids and ranker weights into separate lists
    for ranker in rankers:
        rankers_qid.append(ranker['query'])
        rankers_w.append(ranker['weights'])

    # construct the weight matrix for the rankers
    # (will be used for the Euclidean distance metric)
    rankers_wnp = np.array(rankers_w)
    # find the dissimilarity matrix
    diss_r = squareform(pdist(rankers_wnp))

    # sample a subset of rankers to do the correlation plots
    # (too slow to do it everywhere)
    sampled_rankers = random.sample(range(len(rankers)), 100)

    combs = []
    # create all possible combinations between the sampled subset
    for elem in combinations(sampled_rankers, 2):
        combs.append(elem)

    # get the euclidean distances between them
    first = [i[0] for i in combs]
    second = [i[1] for i in combs]

    vals_euclidean = diss_r[first, second]
    print vals_euclidean.shape

    kendall_t, ndcg_diff, pair_prefs = [], [], []
    # create the ranking functions and start the estimation
    # of the remaining metrics
    cnt = 0
    # for each pair
    for i, j in zip(first, second):
        cnt += 1
        print 'Pair', cnt, 'out of', len(first)
        # make the weights into a Lerot recognizable format
        w1 = str(rankers_w[i].tolist()).strip('[]')
        w2 = str(rankers_w[j].tolist()).strip('[]')

        if typedata == 'MSLR':
            ranker_1 = ProbabilisticRankingFunction('3', 'random', 136, w1, sample='sample_unit_sphere')
            ranker_2 = ProbabilisticRankingFunction('3', 'random', 136, w2, sample='sample_unit_sphere')
            query_ids = glob.glob('/home/mzoghi/Intent/Data/MSLR/train_per_query/t*')
        elif typedata == 'YLR':
            ranker_1 = ProbabilisticRankingFunction('3', 'random', 700, w1, sample='sample_unit_sphere')
            ranker_2 = ProbabilisticRankingFunction('3', 'random', 700, w2, sample='sample_unit_sphere')
            query_ids = glob.glob('/home/mzoghi/Intent/Data/YLR/train_per_query/q*')

        # sample 100 queries
        tot_q = 100
        sampled = random.sample(query_ids, tot_q)

        # lists to accumulate the metrics
        acc_tau1 = []  # , acc_pval1 = [], []
        acc_ndcg1 = []
        acc_pairpref = np.zeros([len(sampled), 2])
        n_docs = 10
        eval_ndcg = NdcgEval()
        user_model = CascadeUserModel('--p_click 0:0.05,1:0.3,2:0.5,3:0.7,4:0.95 --p_stop 0:0.2,1:0.3,2:0.5,3:0.7,4:0.9')
        pi = ProbabilisticInterleave()
        # for each query from the sampled ones
        for k, q_id in enumerate(sampled):
            #print 'Doing', k+1, 'out of', tot_q

            if typedata == 'MSLR':
                q = query.load_queries(q_id, 136)
            elif typedata == 'YLR':
                q = query.load_queries(q_id, 700)

            # do the pairwise preference estimation first
            qu = q[q.keys()[0]]

            ranker_1.init_ranking(qu)
            ranker_2.init_ranking(qu)

            (interleaved_list, assignments) = pi.interleave(ranker_1,
                                                ranker_2, qu, n_docs)
            c = user_model.get_clicks(interleaved_list, qu.get_labels())
            outcome = pi.infer_outcome(interleaved_list, assignments, c, qu)
            if outcome < 0:  # c1 > c2
                acc_pairpref[k, 0] += 1
            elif outcome > 0:  # c2 > c1
                acc_pairpref[k, 1] += 1
            else:  # tie
                acc_pairpref[k, 1] += 0.5
                acc_pairpref[k, 0] += 0.5

            # do the NDCG difference here
    	    ranks_1 = [doc.get_id() for doc in ranker_1.get_ranking()[0:n_docs]]
    	    ranks_2 = [doc.get_id() for doc in ranker_2.get_ranking()[0:n_docs]]

    	    ndcg1 = eval_ndcg.get_value(ranker_1.get_ranking(), qu.get_labels().tolist(), None, cutoff=n_docs)
    	    ndcg2 = eval_ndcg.get_value(ranker_2.get_ranking(), qu.get_labels().tolist(), None, cutoff=n_docs)

    	    acc_ndcg1.append(abs(ndcg1 - ndcg2))
            #c1 = user_model.get_clicks(ranker_1.get_ranking()[0:n_docs], q[q.keys()[0]].get_labels)
    	    #c2 = user_model.get_clicks(ranker_2.get_ranking()[0:n_docs], q[q.keys()[0]].get_labels)

            # find the kendall's tau here
    	    tau1, _ = kendalltau(ranks_1, ranks_2)

    	    acc_tau1.append(tau1)  # ; acc_pval1.append(pval1)

        # average each metric with the number of queries
        kendall_t.append(sum(acc_tau1)/tot_q)
        ndcg_diff.append(sum(acc_ndcg1)/tot_q)
        mean_pref = np.mean(acc_pairpref, axis=0)
        pair_prefs.append(abs(mean_pref[0] - mean_pref[1]))

    print len(kendall_t)
    print len(ndcg_diff)
    print vals_euclidean.shape[0]

    # save the correlation data and do the plots
    np.savez('CorrelationData', kendall_t=np.array(kendall_t), ndcg_diff=np.array(ndcg_diff), vals_euclidean=vals_euclidean, pair_prefs=np.array(pair_prefs))

    plt.figure(1)
    plt.scatter(vals_euclidean, kendall_t)
    plt.xlabel('Euclidean')
    plt.ylabel('Kendall tau')
    fig1 = plt.gcf()
    fig1.savefig('eucl-ken-'+typedata+'.png', dpi=100)

    plt.figure(2)
    plt.scatter(vals_euclidean, ndcg_diff)
    plt.xlabel('Euclidean')
    plt.ylabel('NDCG difference')
    fig2 = plt.gcf()
    fig2.savefig('eucl-ndcg-'+typedata+'.png', dpi=100)

    plt.figure(3)
    plt.scatter(kendall_t, ndcg_diff)
    plt.xlabel('Kendall tau')
    plt.ylabel('NDCG difference')
    fig3 = plt.gcf()
    fig3.savefig('ken-ndcg-'+typedata+'.png', dpi=100)

    plt.figure(4)
    plt.scatter(pair_prefs, ndcg_diff)
    plt.xlabel('Pairwise preferences')
    plt.ylabel('NDCG difference')
    fig4 = plt.gcf()
    fig4.savefig('pair-ndcg-'+typedata+'.png', dpi=100)

    plt.figure(5)
    plt.scatter(pair_prefs, kendall_t)
    plt.xlabel('Pairwise preferences')
    plt.ylabel('Kendall tau')
    fig5 = plt.gcf()
    fig5.savefig('pair-kendall-'+typedata+'.png', dpi=100)


def measure_against_generic(typedata, query_type='specialized'):
    """
    Function that measures win rate between global and query specific rankers.

    Parameters
    -----------
    typedata : string
        Dataset that will be used. Available options are 'MSLR' and 'YLR'.

    query_type : string, optional
        The queries that the win rate will be measured against. Available options are:
            'specialized' : measures win rate between global and local ranker on the query
                            that the local ranker was trained on.
            'random' : measures the win rate between global and local ranker on a random
                        query from the dataset.

    Output
    -------
    The script saves a bar plot of the win rate between local and global rankers on the
    location: typedata+'_winRateVSGeneric_'+query_type+'.png'.

    The measurements according to which the bar plot was created are also saved
    as: 'saved_winrate_'+query_type+'.pkl'. It is a list of tuples where each tuple
    corresponds to (win_rate_global, win_rate_locals) on a random query. The size
    of the list depends on the number of random queries.
    """
    print 'Starting ' + typedata + '...'
    if typedata == 'MSLR':
        all_q = query.load_queries('/home/mzoghi/Intent/Data/MSLR/all_queries.txt', 136)
        print 'Loaded all the queries.'
        # load the global ranker
        f = gzip.open('/home/mzoghi/Intent/Data/MSLR/MSLR_genericRanker_lws-0.txt.gz')
        yml_file = yaml.load(f.read())
        f.close()
        # construct the ranking function for it
        generic_w = str(np.asarray(yml_file['final_weights']).tolist()).strip('[]')
        generic_ranker = ProbabilisticRankingFunction('3', 'random', 136,
            generic_w, sample='sample_unit_sphere')
        specialized_rankers = glob.glob('/home/mzoghi/lerot-christos/Rankers_pq_all_MSLR/Group*/BestRankers/b*')
    elif typedata == 'YLR':
        all_q = query.load_queries('/home/mzoghi/Intent/Data/YLR/all_queries.txt', 700)
        print 'Loaded all the queries.'
        f = gzip.open('/home/mzoghi/Intent/Data/YLR/YLR_genericRanker_lws-0.txt.gz')
        yml_file = yaml.load(f.read())
        f.close()
        generic_w = str(np.asarray(yml_file['final_weights']).tolist()).strip('[]')
        generic_ranker = ProbabilisticRankingFunction('3', 'random', 700,
            generic_w, sample='sample_unit_sphere')
        specialized_rankers = glob.glob('/home/mzoghi/lerot-christos/rankers_pq_all_YLR/Group*/BestRankers/b*')

    print 'Loaded the generic ranker.'
    # sample a number of queries and do interleaving tests
    # of global and local ranker according to a user model
    n_sample = 100
    random_q = random.sample(all_q.keys(), n_sample)
    num_docs = 10
    pi = ProbabilisticInterleave()
    user_model = CascadeUserModel('--p_click 0:0.05,1:0.3,2:0.5,3:0.7,4:0.95 --p_stop 0:0.2,1:0.3,2:0.5,3:0.7,4:0.9')
    # iterations for each query
    N = 100
    all_q_stats = []
    print 'Sampled a subset of the queries and starting the testing phase...'
    for i, key in enumerate(random_q):
        print 'Query', i + 1, 'out of', n_sample
        special_wins, generic_wins, equal = [0]*3
        q = all_q[key]

        if query_type == 'specialized':
            #print specialized_rankers
            special_path = [i for i in specialized_rankers if '_'+key+'.' in i][0]
        elif query_type == 'random':
            special_path = random.choice(specialized_rankers)

        with open(special_path, 'rb') as handle:
            tmp_w = pickle.load(handle)['weights']

        n_feat = tmp_w.shape[0]
        special_w = str(tmp_w.tolist()).strip('[]')
        special_ranker = ProbabilisticRankingFunction('3', 'random', n_feat,
            special_w, sample='sample_unit_sphere')
        for i in xrange(N):
            print 'Round', i + 1,
            interleaved_list, assignments = pi.interleave(generic_ranker, special_ranker, q, num_docs)
            c = user_model.get_clicks(interleaved_list, q.get_labels())
            outcome = pi.infer_outcome(interleaved_list, assignments, c, q)
            if outcome < 0:  # c1 > c2
                generic_wins += 1
            elif outcome > 0:  # c2 > c1
                special_wins += 1
            else:  # tie
                generic_wins += .5
                special_wins += .5

        print 'Finished.'
        q_res = (generic_wins/N, special_wins/N)
        all_q_stats.append(q_res)

    # bin the data and create the histogram plot
    data = [i[1] for i in all_q_stats]
    plt.hist(data, bins=[0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0])
    plt.xlabel('Win rate')
    plt.ylabel('Number of rankers')
    fig = plt.gcf()
    fig.savefig(typedata+'_winRateVSGeneric_'+query_type+'.png', dpi=100)
    # save the measurements
    with open('saved_winrate_'+query_type+'.pkl', 'wb') as handle:
         pickle.dump(all_q_stats, handle)

if __name__ == '__main__':
    typedata = sys.argv[1]
    # measure_against_generic(typedata, query_type='specialized')
    # measure_against_generic(typedata, query_type='random')
    create_correlation_plots_Eucl_Kend_NDCG_Preference(typedata)
