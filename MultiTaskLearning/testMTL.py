import numpy as np
import cPickle as pkl
from natsort import natsorted
from sklearn.metrics import confusion_matrix
from sklearn.linear_model import LogisticRegression


def transformQueriesToData(queries, typedata='MSLR'):
    """
    Given the queries get the data matrix of query-document pairs. The task is
    simple binary classification. (Relevant - Non relevant)

    Parameters
    -----------
    queries : Lerot Query object
        Contains the query-document feature vectors

    typedata : string, optional
        The dataset that will be used.
        Available options are: 'MSLR' and 'YLR'

    Output
    -------
    data : numpy array
        The data matrix (feature vectors for query-document pairs)

    labels : numpy array
        The binary labels (0 non-relevant, 1 relevant)

    query_assign : list of strings
        List of query IDs where each element corresponds to the query ID
        of the corresponding row of the data matrix.
    """
    print 'transforming queries to data...'
    query_assign = []
    # initialize dimensionality accordingly
    if typedata == 'MSLR':
        data = np.zeros((1, 136))
    elif typedata == 'YLR':
        data = np.zeros((1, 700))

    labels = np.zeros(1)
    # sweep through the queries and populate the data
    for key in natsorted(queries.keys()):
        q = queries[key]
        query_assign.extend([key]*len(q.get_labels()))
        data = np.vstack((data, q.get_feature_vectors()))
        labels = np.hstack((labels, q.get_labels()))

    # remove useless first row
    data = data[1:, :]
    labels = labels[1:]
    print 'transformed queries to data.'
    return data, labels, query_assign


def assignQueriesToTasks(data, labels, query_assign, q_clust, how_many_test=100):
    """
    Function that creates smaller task dependent datasets out of the original dataset.

    Parameters
    -----------
    data : numpy array
        Original data matrix

    labels : numpy array
        Labels of the original data matrix

    query_assign : list of strings
        List that contains the mapping of rows of the data matrix to query IDs

    q_clust : dictionary
        Dictionary that contains key, value pairs where the key is the
        query ID and the value is the cluster that it is assigned. Contains
        one extra field 'numofclusters' which denotes the total number of clusters.

    how_many_test : int, optional
        Integer that denotes how many feature vectors will be used for testing.
        Defaults to 100.

    Output
    -------
    tasks_train : list of lists of numpy arrays
        Each sublist corresponds to a task and where the first element
        of the sublist is the feature vectors and the second element in
        the sublist is the labels. These will be used for training

    tasks_test : list of lists of numpy arrays
        Similar to tasks_train but these will be used for testing
    """
    tasks_train, tasks_test = [], []
    q_keys = natsorted(q_clust.keys()[1:])
    print 'Total number of clusters:', q_clust['numofclusters']
    for i in xrange(q_clust['numofclusters']):
        qs_incl = [key for key in q_keys if q_clust[key] == i+1]
        # get all the indices corresponding to this task (each cluster is considered
        # a separate task.)
        indices = [j for j in xrange(len(query_assign)) if query_assign[j] in qs_incl]
        # get the train and test indices
        idtrain = indices[0:len(indices)-how_many_test]
        idtest = indices[len(indices)-how_many_test:len(indices)]
        # add the corresponding feature vectors and labels
        tasks_train.append([data[idtrain, :], labels[idtrain]])
        tasks_test.append([data[idtest, :], labels[idtest]])

    return tasks_train, tasks_test


def doMTL(per_task_data, per_task_data_test, typedata='MSLR'):
    """
    Function that performs multi-task learning and comparison against single task.
    Python bindings for the Shogun ML toolbox are required for the multi task learning part.

    Parameters
    -----------
    per_task_data : list of lists of numpy arrays
        Each sublist corresponds to a task and where the first element
        of the sublist is the feature vectors and the second element in
        the sublist is the labels. These will be used for training.

    per_task_data_test : list of lists of numpy arrays
        Similar to per_task_data but these will be used for testing

    typedata : string
        Corresponds to the dataset that will be used for multitask learning
        Currently only 'MSLR' is available
    """

    #from modshogun import MulticlassLabels, BinaryLabels, RealFeatures, Task, TaskGroup, MultitaskL12LogisticRegression
    from modshogun import BinaryLabels, RealFeatures, Task, TaskGroup, MultitaskL12LogisticRegression
    from modshogun import MultitaskTraceLogisticRegression, MultitaskLogisticRegression
    task_indices = []
    # shogun wants them organized in a specific way...
    if typedata == 'MSLR':
        data = np.zeros([1, 136])
        labels = np.zeros(1)
        cnt = 0
        for task in per_task_data:
            task_size = task[0].shape[0]
            data = np.vstack((data, task[0]))
            labels = np.hstack((labels, task[1]))
            task_indices.append((cnt, cnt+task_size))
            cnt += task_size

        data = data[1:, :]
        labels = labels[1:]

    print 'Data size:', data.shape
    # shogun wants the instances on the columns and the features on the rows
    features = RealFeatures(data.T)
	# shogun multitask logistic regression works only for binary labels
	# at the moment so convert mutliclass to binary (relevant-non relevant)
    labels_binary = [1. if i > 0 else -1. for i in labels]

	# train the single task baseline
    clf = LogisticRegression()
    clf.fit(data, labels_binary)

	# change also the labels on test
    for task in per_task_data_test:
		task[1] = [1. if i > 0 else -1. for i in task[1]]
        #labels_ = MulticlassLabels(labels)
    labels_ = BinaryLabels(np.asarray(labels_binary))
    # create a shogun task group
    tasks = TaskGroup()
    # test_data = np.zeros([1, 136])
    # shogun wants indices
    for begin, end in task_indices:
        tasks.append_task(Task(begin, end))
        #mtlr = MultitaskL12LogisticRegression(0.1, 0.1, features, labels_, tasks)
        #mtlr = MultitaskTraceLogisticRegression(0.1, features, labels_, tasks)
	mtlr = MultitaskLogisticRegression(0.5, features, labels_, tasks)
	print 'Initialized object'
	mtlr.set_regularization(1)  # boolean param for regularization
    mtlr.set_tolerance(1e-2)
    mtlr.set_max_iter(1000)
    print 'Successfully setted parameters'
    mtlr.train()
    print 'Finished training'
    for k in xrange(len(per_task_data_test)):
        print 'Testing for task', k+1
        mtlr.set_current_task(k)
        # get predictions from the multi task algorithm
        res = mtlr.apply_binary(RealFeatures(per_task_data_test[k][0].T)).get_labels()
        # get the results from the baseline single task
        res_base = clf.predict(per_task_data_test[k][0])
        # convert to 0,1 to -1, 1
        res_base = [i if i == 1 else -1 for i in res_base]
        print res
        print '----'
        print res_base
        print '----'
        print per_task_data_test[k][1]
        # loss for multi task
        loss = [0 if res[i] == per_task_data_test[k][1][i] else 1 for i in xrange(res.shape[0])]
        # loss for single task
        loss_base = [0 if res_base[i] == per_task_data_test[k][1][i] else 1 for i in xrange(res.shape[0])]
        print 'for multitask:', sum(loss), 'were different'
        print 'for singletask:', sum(loss_base), 'were different'
        print 'multitask confusion matrix:'
        print confusion_matrix(res, per_task_data_test[k][1])
        print 'baseline confusion matrix:'
        print confusion_matrix(res_base, per_task_data_test[k][1])
        print

if __name__ == '__main__':
    with open('/home/mzoghi/sampled_queries_MSLR.pkl', 'rb') as handle:
        queries = pkl.load(handle)

    with open('qclusters_MSLR.pkl', 'rb') as handle:
        q_clust = pkl.load(handle)
    #q_clust = {}   # needs to be imported from clustering_experiments.py
    print 'transforming queries to training data...'
    data, labels, query_assign = transformQueriesToData(queries)
    print 'assigning queries to tasks...'
    tasks_train, tasks_test = assignQueriesToTasks(data, labels, query_assign, q_clust)
    print 'doing multi task learning...'
    doMTL(tasks_train, tasks_test, typedata='MSLR')
