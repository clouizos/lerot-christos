#!/usr/bin/env python
# -*- coding: utf8 -*-

import os
import glob
import argparse
import numpy as np
import cPickle as pickle

from lambdamart import LambdaMART
from lerot.evaluation import NdcgEval


def get_model_description(s):
    '''
    Turn the specified string into the LamdaMART
    model description.
    '''
    description = 'LambdaMART Parameters: %(T)s trees, %(L)s leaves, shrinkage: %(S)s, validation: %(V)s, max. grade: %(G)s, estop: %(E)s, metric: %(M)s.'
    return description % dict((parameter[:1], parameter[1:]) for parameter in s.strip().split('_'))


def get_dataset_name(filepath):
    '''
    Return the name of the dataset from its filepath (i.e. directories
    and extensions are removed).
    '''
    filename = os.path.basename(filepath)
    return filename if filename.index('.') < 0 else filename[:filename.index('.')]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
    parser.add_argument('--prefix', default='query_dataset_cluster_', help="specify the prefix for dataset/model filenames (default: '%(default)s')")
    parser.add_argument('--scores', help='specify (optional) output directory for scores assigned by a model to each test query (for debugging)')
    parser.add_argument('--output-models', help='specify (optional) output directory for saving the models in RankLib-like XML format (for debugging)')
    parser.add_argument('datasets', help='specify the input directory with test sets for models evaluations')
    parser.add_argument('models', help="specify the input directory with trained RankLib's LambdaMART models")
    parser.add_argument('report', help='specify the output file for evaluation report')

    arguments = parser.parse_args()

    prefix = arguments.prefix

    datasets_dir = arguments.datasets
    models_dir = arguments.models
    models_output_dir = arguments.output_models

    scores_dir = arguments.scores
    report_filepath = arguments.report

    # We can evaluate models only using NDCG here.
    evalNDCG = NdcgEval()

    with open(report_filepath, 'w') as report_file:
        # Loop through every model in specify directory.
        for model_name in sorted(os.listdir(models_dir)):

            cutoff = int(model_name[model_name.index('@') + 1:])

            # Check the model is supposed to be evaluated using NDCG, if not skip it.
            if not model_name.endswith('MNDCG@' + str(cutoff)):
                warnings.warn('Skipping evaluation of model: %s. It is not using NDCG metric.')
                continue

            model_directory_path = os.path.join(models_dir, model_name)

            curr_scores_output_dir = None
            curr_models_output_dir = None

            # Should an ouput directory for saving scores of every testing query be create?
            if scores_dir is not None:
                curr_scores_output_dir = os.path.join(scores_dir, model_name)

                if not os.path.exists(curr_scores_output_dir):
                    os.makedirs(curr_scores_output_dir)

            # Should an ouput directory for saving (loaded) LambdaMART models be create?
            if models_output_dir is not None:
                curr_models_output_dir = os.path.join(models_output_dir, model_name)

                if not os.path.exists(curr_models_output_dir):
                    os.makedirs(curr_models_output_dir)

            # Parse the LambdaMART model parameters from the name.
            model_description = get_model_description(model_name) 

            # Load the global LambdaMART model.
            model_global = LambdaMART.load_from_text(os.path.join(model_directory_path, prefix + '0.model.txt'))

            # Immediatelly serialize the model using the Python's code.
            if curr_models_output_dir is not None:
                model_global.save_to_text(os.path.join(curr_models_output_dir, prefix + '0.model.txt'))

            table_datasets = []
            table_globals = []
            table_locals = []
            table_winners = []

            # The test datasets filepaths: all file within specified dataset directory with given prefix and '.test.pkl' suffix.
            for test_dataset_filepath in sorted(glob.glob(os.path.join(datasets_dir, prefix + '*.test.pkl'))):

                curr_dataset_name = get_dataset_name(test_dataset_filepath)

                # We need to skip the dataset used to test the global model.
                if curr_dataset_name.endswith('_0'):
                    continue

                table_datasets.append(curr_dataset_name)
    
                # Load the local LambdaMART model.
                model_local = LambdaMART.load_from_text(os.path.join(model_directory_path, curr_dataset_name + '.model.txt'))
        
                # Immediatelly serialize the model using the Python's code.
                if curr_models_output_dir is not None:
                    model_local.save_to_text(os.path.join(curr_models_output_dir, curr_dataset_name + '.model.txt'))

                # Load testing queries associated with the current local model.
                with open(test_dataset_filepath) as ifile:
                    queries = pickle.load(ifile)

                ndcg_local = 0.0
                ndcg_global = 0.0

                # Since we will be appending to the scores output file, we need to remove ahead of time.
                if curr_scores_output_dir is not None:
                    if os.path.exists(os.path.join(curr_scores_output_dir, curr_dataset_name + '.global.txt')):
                        os.remove(os.path.join(curr_scores_output_dir, curr_dataset_name + '.global.txt'))

                    if os.path.exists(os.path.join(curr_scores_output_dir, curr_dataset_name + '.local.txt')):
                        os.remove(os.path.join(curr_scores_output_dir, curr_dataset_name + '.local.txt'))
 
                # Compute NDCG for each testing query and compute the score for every document in its list
                # by global and local models that have been previously trained by RankLib on associated 
                # training datasets.
                for query in queries:
                    model_global.init_ranking(query)
                    model_local.init_ranking(query)

                    ndcg_global += evalNDCG.get_value(model_global.get_ranking(), list(query.get_labels()), None, cutoff) 
                    ndcg_local += evalNDCG.get_value(model_local.get_ranking(), list(query.get_labels()), None, cutoff)

                    # Compute the scores for each document if requested.
                    if curr_scores_output_dir is not None:
                        scores_global = model_global.get_scores()
                        scores_local = model_local.get_scores()

                        with open(os.path.join(curr_scores_output_dir, curr_dataset_name + '.global.txt'), 'a') as ofile:
                            for pos, docid in enumerate(query.get_docids()):
                                ofile.write('%s\t%d\t%.16f\n' % (query.get_qid(), docid.get_id(), scores_global[pos]))

                        with open(os.path.join(curr_scores_output_dir, curr_dataset_name + '.local.txt'), 'a') as ofile:
                            for pos, docid in enumerate(query.get_docids()):
                                ofile.write('%s\t%d\t%.16f\n' % (query.get_qid(), docid.get_id(), scores_local[pos]))

                # Compute the final NDCG for global and local LambdaMART models for the current dataset.
                ndcg_local /= len(queries)
                ndcg_global /= len(queries)

                # Determine the content of the last table column (winner of comparison).
                if ndcg_global > ndcg_local:
                    table_winners.append('Global')
                elif ndcg_global < ndcg_local:
                    table_winners.append('Local')
                else:
                    table_winners.append('Draw')

                table_globals.append('%.4f' % ndcg_global)
                table_locals.append('%.4f' % ndcg_local)

            # Calculate the table column widths (w/o padding).
            width_col1 = max(max(map(len, table_datasets)), len('Datasets'))
            width_col2 = max(max(map(len, table_globals)), len('Global'))
            width_col3 = max(max(map(len, table_locals)), len('Local'))
            width_col4 = len('Winner')

            table_row_format = '| %%%ds | %%%ds | %%%ds | %%%ds |\n' % (width_col1, width_col2, width_col3, width_col4)

            # Account for the cell padding.
            width_col1 += 2
            width_col2 += 2
            width_col3 += 2
            width_col4 += 2

            table_line_sep = '+%s+%s+%s+%s+\n' % ('-' * width_col1, '-' * width_col2, '-' * width_col3, '-' * width_col4)

            # Print the LamdaMART description.
            report_file.write(model_description)
            report_file.write('\n')

            # Print the table header.
            report_file.write(table_line_sep)
            report_file.write(table_row_format % ('Dataset', 'Global', 'Local', 'Winner'))
            report_file.write(table_line_sep)

            # Print the table content.
            for items in zip(table_datasets, table_globals, table_locals, table_winners):
                report_file.write(table_row_format % items)
                report_file.write(table_line_sep)

            report_file.write('\n')
