from clustering_experiments import parse_rankers, parse_queries, estimate_kendall_dist_mat, print_now
import sys
from numpy import *
from multiprocessing import Pool
from lerot import query
import re
from glob import glob
from os import path


def get_subset(subset, all_queries):
    """
    Function that returns a subset of the original set of queries

    Parameters
    -----------
    subset : list of strings
        Query IDs that you want to keep from the original query set

    all_queries : Lerot Query object
        The original set of queries

    Output
    -------
    queries : Lerot Query object
        Lerot Query object that contains only the subset of the queries
        that was specified
    """
    new_q = {}
    for id_q in subset:
        new_q[id_q] = all_queries[id_q]
    queries = query.Queries(new_q, 136, read_q=False)
    return queries


def load_data(typedata, sample_size='all', row_args=[], col_args=[]):
    """
    Function that loads the given queries and their corresponding rankers

    Parameters
    -----------
    typedata : string
        The dataset that you will load. Available options 'MSLR' and 'YLR'.
        Currently only 'MSLR' is supported.

    sampled_size : int or string, optional
        How many queries will be sampled. 'all' for all of the queries or
        and integer for integer amount of queries. Defaults to 'all'

    row_args : list of ints, optional
        Corresponds to the row rankers that will be used (rankers are sorted
        in ascending order according to their query ID). Defaults to an empty list.

    col_args : list of ints, optional
        Corresponds to the column rankers that will be used (rankers are sorted
        in ascending order according to their query ID). (Can be different from row_args)
        Defaults to an empty list.

    Output
    -------
    row_full_rankers : list of dictionaries
        Returns a list of dictionaries where each dictionary corresponds to a row ranker.
        See documentation on parse_rankers for more information.

    col_full_rankers : list of dictionaries
        Returns a list of dictionaries where each dictionary corresponds to a column ranker.
        See documentation on parse_rankers for more information.

    row_q : list of strings
        A list of query IDs that corresponds to the row rankers that were used

    col_q : list of strings
        A list of query IDs that corresponds to the column rankers that were used

    row_queries : Lerot Query object
        A Lerot Query object that corresponds to the query-document feature vectors
        for the row rankers

    col_queries : Lerot Query object
        A Lerot Query object that corresponds to the query-document feature vectors
        for the column rankers
    """
    print_now('loading queries...')
    # load the queries and sort their keys in ascending order
    # (correct matching against indices in the distance matrix)
    queries = parse_queries(typedata, [])
    sorted_q = sorted(queries.keys(), key=lambda x: int(x))

    # parse the query IDs corresponding to the given ranker indices
    row_q = sorted_q[row_args[0]:row_args[1]]
    col_q = sorted_q[col_args[0]:col_args[1]]
    print_now('row_q of length '+str(len(row_q))+':\n'+str(row_q))
    print_now('col_q of length '+str(len(col_q))+':\n'+str(col_q))

    # parse the query-document feature vectors of the subset
    row_queries = get_subset(row_q, queries)
    col_queries = get_subset(col_q, queries)

    print_now('loading rankers..')
    # Load corresponding rankers
    row_rankers, _, row_full_rankers, row_q_ids = \
                        parse_rankers(typedata, sample_size=sample_size,
                                      np_random_seed=1372,
                                      to_keep=row_q)

    col_rankers, _, col_full_rankers, col_q_ids = \
                        parse_rankers(typedata, sample_size=sample_size,
                                      np_random_seed=1372,
                                      to_keep=col_q)

    print_now('Done')
    return row_full_rankers, col_full_rankers, \
           row_q, col_q, \
           row_queries, col_queries


def run_estimate_kendall_dist_mat(row_rankers, col_rankers,
                                  row_q_ids, col_q_ids,
                                  row_queries, col_queries,
                                  typedata,
                                  orig_row=[], orig_col=[]):
    """
    Function that estimates a piece of the full Kendall distance matrix
    that corresponds to the given subset of rankers and queries.

    Parameters
    -----------
    row_rankers : list of dictionaries
        Each dictionary corresponds to a row ranker. For information
        on the keys see parse_rankers.py.

    col_rankers : list of dictionaries
        Each dictionary corresponds to a column ranker. For information
        on the keys see parse_rankers.py.

    row_q_ids : list of strings
        List of query IDs for the specified row rankers.

    col_q_ids : list of strings
        List of query IDs for the specified column rankers.

    row_queries : Lerot Query object
        Lerot Query object that contains the query-document feature vectors
        for the row rankers.

    col_queries : Lerot Query object
        Lerot Query object that contains the query-document feature vectors
        for the column rankers.

    typedata : string
        Corresponds to the dataset that is used. Currently only 'MSLR' is
        available.

    orig_row : list of two ints, optional
        First integer corresponds to the start of the rows and the second
        corresponds to the end of the rows. Default is an empty list.

    orig_col : list, optional
        First integer corresponds to the start of the columns and the second
        corresponds to the end of the columns. Default is an empty list.

    Output
    -------
    Saves the piece of the distance matrix in the "/home/mzoghi/Intent/Output_Dist_Mtx/"
    directory with the name typedata+"_BeginRow_EndRow_BeginColumn_EndColumn", as an npz file.
    """
    print_now("row_rankers['query'] = "+str([r['query'] for r in row_rankers]))
    print_now("col_rankers['query'] = "+str([r['query'] for r in col_rankers]))
    distance_matrix = \
        estimate_kendall_dist_mat(row_rankers, col_rankers,
                                  row_queries, col_queries, cutoff=10)

    savez("/home/mzoghi/Intent/Output_Dist_Mtx/" + \
          typedata+"_".join(map(str, (orig_row+orig_col))),
          row_queries=array(row_q_ids),
          col_queries=array(col_q_ids),
          row_rankers_queries=array([r['query'] for r in row_rankers]),
          col_rankers_queries=array([r['query'] for r in col_rankers]),
          distance_matrix=distance_matrix)


def gather_kandall_dist_mat(DIR="/home/mzoghi/Intent/Output_Dist_Mtx/"):
    """
    Function that gathers all the separately estimated pieces of the full Kendall
    distance matrix and creates the original matrix.

    Parameters
    -----------
    DIR : string
      Directory of the pieces of the full distance matrix. Default is
      "/home/mzoghi/Intent/Output_Dist_Mtx/"

    Output
    -------
    DistMat : numpy array
        The reconstructed full distance matrix.
    """
    Fs = glob(path.join(DIR, "*.npz"))
    Limits = [tuple([int(d) for d in re.findall(r'\d+', f)]) for f in Fs]
    row_max = array(Limits)[:, 1].max()
    col_max = array(Limits)[:, 3].max()
    DistMat = zeros([row_max, col_max])
    Row_q = zeros(row_max)
    Col_q = zeros(col_max)
    for ind in range(len(Fs)):
        if (ind+1) % 100 == 0:
            print_now(str(ind+1)+' out of '+str(len(Fs)))
        f = Fs[ind]
        l = Limits[ind]
        data = load(f)
        DM = data['distance_matrix']
        DistMat[l[0]:l[1], l[2]:l[3]] = DM
        if (data['row_rankers_queries'] != data['row_queries']).any():
            print f, "- data['row_rankers_queries'] != data['row_queries'] at",
            print nonzero(data['row_rankers_queries'] != data['row_queries'])[0]
        if (data['col_rankers_queries'] != data['col_queries']).any():
            print f, "- data['col_rankers_queries'] != data['col_queries'] at",
            print nonzero(data['col_rankers_queries'] != data['col_queries'])[0]
        row_queries = data['row_rankers_queries'].astype(float)
        col_queries = data['col_rankers_queries'].astype(float)
        if (Row_q[l[0]:l[1]] == 0).all():
            Row_q[l[0]:l[1]] = row_queries
        elif (Row_q[l[0]:l[1]] != row_queries).any():
            print "ERROR at", f, "- Row_q["+str(l[0])+":"+str(l[1])+"] != row_queries at",
            print nonzero(Row_q[l[0]:l[1]] != row_queries)[0]
        if (Col_q[l[2]:l[3]] == 0).all():
            Col_q[l[2]:l[3]] = col_queries
        elif (Col_q[l[2]:l[3]] != col_queries).any():
            print "ERROR at", f, "- Col_q["+str(l[2])+":"+str(l[3])+"] != col_queries at",
            print nonzero(Col_q[l[2]:l[3]] != col_queries)[0]
        data.close()
    return DistMat


if __name__ == '__main__':
    # get the necessary arguments from the command line
    typedata = sys.argv[1]  # the dataset
    row_min = int(sys.argv[2])  # from which row we start
    row_max = int(sys.argv[3])  # on which row we end
    col_min = int(sys.argv[4])  # from which column we start
    col_max = int(sys.argv[5])   # on which column we end
    nCores = int(sys.argv[6])   # how many cores does the machine have
                                # columns will be distributed among the cores
    # start the pool
    pool = Pool(processes=nCores)

    # parse only the specific rankers and queries
    row_rankers, col_rankers, \
    row_q_ids, col_q_ids, \
    row_queries, col_queries = load_data(typedata, sample_size='all',
                                         row_args=[row_min, row_max],
                                         col_args=[col_min, col_max])

    # distribute the columns
    step_col = linspace(0, len(col_rankers), nCores+1).astype(int)
    if nCores == 1:
      run_estimate_kendall_dist_mat( row_rankers,
                                     col_rankers,
                                     row_q_ids,
                                     col_q_ids,
                                     row_queries,
                                     col_queries,
                                     typedata,
                                     [row_min, row_max],
                                     [col_min, col_min])
    else:
      for i in xrange(step_col.shape[0] - 1):
          print "Iteration number", i
          col_queries_subset = get_subset(col_q_ids[step_col[i]:step_col[i+1]],
                                          col_queries)
          pool.apply_async(run_estimate_kendall_dist_mat, (row_rankers,
                                         col_rankers[step_col[i]:step_col[i+1]],
                                         row_q_ids,
                                         col_q_ids[step_col[i]:step_col[i+1]],
                                         row_queries,
                                         col_queries_subset,
                                         [row_min, row_max],
                                         [step_col[i]+col_min,
                                          step_col[i+1]+col_min]))
      pool.close()
      pool.join()
