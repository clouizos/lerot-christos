import glob
from natsort import natsorted
import gzip
import yaml
import numpy as np
import os
import cPickle as pickle
import sys
from multiprocessing import Pool


def chunks(lst, n):
    """
    Splits a list into n parts

    Parameters
    -----------
    lst : list
        The list that will be splitted

    n : int
        The number of parts that you want
        the list to be splitted into

    Output
    -------
    A list of lists where each sublist corresponds to
    a part of the original list
    """
    return [lst[i::n] for i in xrange(n)]


def rankers_for_q(all_rankers, q_id):
    """
    Returns all the ranker paths for a specific query ID

    Parameters
    -----------
    all_rankers : list of strings
        The list that contatins the paths of all the available rankers

    q_id : string
        The query ID that you want to parse its rankers

    Output
    -------
    A list of paths for rankers for the query with ID q_id.
    """
    return [q for q in all_rankers if '_'+q_id+'_' in q]


def parse_rankers_for_q(ranker_dir, q_id, path_save_dir, type_data):
    """
    Function that parses the rankers over different runs for a specific query.

    Parameters
    ------------
    ranker_dir : string
        The directory that contains the rankers.

    q_id : string
        The query that you want to parse the rankers for.

    path_save_dir : string
        The directory that you want to save the rankers.

    type_data : string
        Denotes the dataset that will be used.
        Available options are 'MSLR' and 'YLR'

    Output
    -------
    A pickle is saved in path_save_dir with the name 'rankers_'+q_id+'.pickle'. This
    contains a list of dictionaries where each dictionary corresponds to a ranker from
    a specific run. The fields in this dictionary are:
        'path': The original path of the ranker,
        'query': The query of the ranker
        'run': On which 'run' the ranker was created (we created a ranker for a query by picking the best one
                among a set of trained rankers, each one of them obtained from a separate 'run')
        'system': The system that was used to train the ranker (e.g. Listwise Learning System - 'lws')
        'weights': A numpy array that contains the weights of the ranker
    """
    rankers = []
    print 'Started query', q_id
    if type_data != 'YLR':
        all_rankers = glob.glob(ranker_dir+'train_*.gz')
    else:
        all_rankers = glob.glob(ranker_dir+'query1_*.gz')

    # get all the paths for the rankers and sort them according
    # to the runs
    rankers_q = natsorted(rankers_for_q(all_rankers, q_id))

    for j, ranker_path in enumerate(rankers_q):
        #print 'Parsing', j + 1, 'out of', len(rankers_q)
        # construct for each ranker a dictionary with useful information
        ranker = {}
        ranker['path'] = ranker_path
        f = gzip.open(ranker_path, 'rb')
        yml_file = yaml.load(f.read())
        f.close()
        ranker['weights'] = np.asarray(yml_file['final_weights'])
        ranker['weights'] = ranker['weights']/np.linalg.norm(ranker['weights'])
        #print ranker_path.split()

    # NOTE: take care with the paths since the split locations will be different
    #       for different paths
	ranker_args = ranker_path.split('/')[-1].split('_')
        if type_data == 'YLR':
	    ranker['query'] = ranker_args[1]
	    ranker['system'], ranker['run'] = ranker_args[2].split('.')[0].split('-')
        elif type_data == 'MSLR':
            ranker['query'] = ranker_args[2]
            ranker['system'], ranker['run'] = ranker_args[3].split('.')[0].split('-')
        rankers.append(ranker)

    print 'Saving rankers for,', q_id, '...'
    path_save = 'rankers_'+q_id+'.pickle'
    with open(path_save_dir+'/'+path_save, 'wb') as handle:
            pickle.dump(rankers, handle)
    print 'Saved.'


def helper_save(input_p):
    """Helper function for the parallelized parsing of the rankers"""
    queries, folder_save, ranker_dir, type_data = input_p
    for i, q_id in enumerate(queries):
        print 'Query', i + 1, 'out of', len(queries)
        parse_rankers_for_q(ranker_dir, q_id, folder_save, type_data)


if __name__ == '__main__':
    # parse the necessary arguments from the command line
    # take care with the paths since the split locations
    # will be different for different paths
    ranker_dir = sys.argv[1]  # 'Output_q_all/'
    ranker_out_dir = sys.argv[2]
    group_id = ranker_dir.split('/')[-1]
    splitted_list = ranker_out_dir.split('/')
    if splitted_list[-1] == '':
        to_split = splitted_list[-2]
    else:
        to_split = splitted_list[-1]
    type_data = to_split.split('_')[-1]

    print 'Dataset:', type_data
    print 'Doing group:', group_id

    try:
        os.makedirs(ranker_out_dir+group_id)
    except:
        print 'Folder already created!'
        pass

    print 'Splitting queries into parts...'
    if type_data != 'YLR':
        queries = list(set([elem.split('/')[-1].split('_')[-2] for elem in glob.glob(ranker_dir+'/t*gz')]))
    else:
        queries = list(set([elem.split('/')[-1].split('_')[-2] for elem in glob.glob(ranker_dir+'/q*gz')]))

    #print queries
    workers = 16
    # since we have 16 cores
    chunks_qs = chunks(queries, workers)
    params = zip(chunks_qs, [ranker_out_dir+group_id]*len(chunks_qs), [ranker_dir+'/']*len(chunks_qs), [type_data]*len(chunks_qs))
    #print params

    #print
    #for param in params:
    #    print len(param[0]), param[1], param[2], param[3]
    #    print
    print 'Initializing pool...'
    pool = Pool()
    print 'Running...'
    pool.map(helper_save, params)
    #pool.join()
    pool.close()
    pool.join()
    print 'Done...'
