#!/usr/bin/env python

'''
Creates a training and test sets for each cluster of queries defined
by the given query to cluster mapping.
'''

import os
import random
import argparse
import cPickle as _pickle

from itertools import groupby, chain
from lerot.query import Queries


def pickle(filepath, obj):
    with open(filepath, 'wb') as ofile:
        _pickle.dump(obj, ofile, protocol=-1)


def to_lerot_queries(queries):
    '''
    Lerot Queries object wants a mapping (QUERY_ID -> QUERY)
    instead of just a simple list.
    '''
    return dict((query.get_qid(), query) for query in queries)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
    parser.add_argument('--ts', type=float, default=0.2, help='specify the fraction of queries reserved for testing')
    parser.add_argument('--seed', type=int, default=None, help='specify the seed for random number generator (used for shuffling queries)')
    parser.add_argument('--no-shuffle', action='store_true', help='specify no shuffling of the dataset samples should be made')
    parser.add_argument('--prefix', default='query_dataset_cluster_', help="specify the prefix for output filenames (default: '%(default)s')")
    parser.add_argument('queries', help='specify the input clustering as pickled mapping QUERY_ID -> CLUSTER_ID')
    parser.add_argument('clusters', help='specify the output clustering with filtered clusters')
    parser.add_argument('output', help='specify the output directory for training/test sets')

    arguments = parser.parse_args()

    # Load the queries (lerot.query.Queries).
    with open(arguments.queries) as ifile:
        queries = _pickle.load(ifile)

    # Load the clustering (mapping from query IDs to cluster IDs).
    with open(arguments.clusters) as ifile:
        qid2cid = _pickle.load(ifile)

    # Make sure output directory exists.
    if not os.path.exists(arguments.output):
        os.makedirs(arguments.output)

    cluster_queries = []

    f_get_qid = lambda (qid, cid): qid
    f_get_cid = lambda (qid, cid): cid

    # Remove the silly item (if still present in the cluster mapping).
    if 'numofclusters' in qid2cid:
        del qid2cid['numofclusters']

    for cid, qids in groupby(sorted(qid2cid.iteritems(), key=f_get_cid), f_get_cid):
        assert cid - 1 == len(cluster_queries), 'Cluster IDs are not continuous (starting from 1).'
        cluster_queries.append([queries[qid] for qid in map(f_get_qid, qids)])

    # Shuffle the queries (if --no-shuffle was not specified) to get new training/test splits every time.
    if not arguments.no_shuffle:
        random.seed(arguments.seed)
        for qs in cluster_queries:
            random.shuffle(qs)

    # Defines the split indices for dividing the datasets on training and test sets.
    splits = map(lambda sz: max(1, int(arguments.ts * sz)), map(len, cluster_queries))

    # Devide the cluster queries into training and test parts.
    train_cluster_queries = [cluster_queries[cid][split:] for cid, split in enumerate(splits)]
    test_cluster_queries = [cluster_queries[cid][:split] for cid, split in enumerate(splits)]

    n_features = queries.__num_features__

    # Save the training and test sets for the global model.
    pickle(os.path.join(arguments.output, arguments.prefix + '0.train.pkl'), Queries(to_lerot_queries(chain(*train_cluster_queries)), n_features, read_q=False))
    pickle(os.path.join(arguments.output, arguments.prefix + '0.test.pkl'), Queries(to_lerot_queries(chain(*test_cluster_queries)), n_features, read_q=False))

    # Save the training and test sets for the local (cluster) models along with the statistics.
    with open(os.path.join(arguments.output, 'statistics.nfo'), 'w') as ofile:
        for cid, (train_queries, test_queries) in enumerate(zip(train_cluster_queries, test_cluster_queries)):
            dataset_name = arguments.prefix + str(cid + 1)

            pickle(os.path.join(arguments.output, dataset_name + '.train.pkl'), Queries(to_lerot_queries(train_queries), n_features, read_q=False))
            pickle(os.path.join(arguments.output, dataset_name + '.test.pkl'), Queries(to_lerot_queries(test_queries), n_features, read_q=False))

            ofile.write('%s (train):\t%d\t%d\n' % (dataset_name, len(train_queries), sum([query.get_document_count() for query in train_queries])))
            ofile.write('%s (test): \t%d\t%d\n' % (dataset_name, len(test_queries), sum([query.get_document_count() for query in test_queries])))
