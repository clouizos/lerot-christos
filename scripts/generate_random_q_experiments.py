import random
import glob
import sys
import os

"""
This script generates configuration files for creating
specialized rankers for the queries
"""

# what dataset you will use
type_data = sys.argv[3]
# get the corresponding directories
directory = '/home/mzoghi/Intent/Data/'+type_data+'/'
# where you will save the configuration files
directory_out = '/home/mzoghi/Intent/'
# where the txt files for the data for each query are located
path_to_q = directory+'train_per_query/'
# where the configuration files will be saved
path_to_c_r = 'config/'

# get all queries in the specified file
queries = glob.glob(path_to_q+'*.txt')
# get the template configuration file
template = sys.argv[2]
# specify some parameters
numruns = '50'
numfeatures = '136'
numqueries = '10000'

# what system you will use
systems = ['lws']   # ['lws', 'lcps', 'pls']
# if you do not want all the queries then sample a subset
if sys.argv[1] != 'all':
    num_q = int(sys.argv[1])  # the amount of queries is the first argument for the script
    path_to_c_w = directory+'Config_pq_'+str(num_q)+'_'+type_data+'/'
    out_dir = directory_out+'Output_q_'+str(num_q)+'_'+type_data+'/'
    random_q = random.sample(queries, num_q)  # num_q = 100
# otherwise create configuration files for everything
else:
    random_q = queries
    path_to_c_w = directory+'Config_pq_all/'
    out_dir = directory_out+'Output_q_all_'+type_data+'/'

# make folders as required
if not os.path.exists(path_to_c_w):
    os.makedirs(path_to_c_w)
if not os.path.exists(out_dir):
    os.makedirs(out_dir)

# for each query and system fill the specified
# fields of the template configuration file
for q in random_q:
    for system in systems:
        # get more parameters according to the system that will be used
        if system is 'lws':
            sys_use = 'retrieval_system.ListwiseLearningSystem'
            exp_use = 'experiment.LearningExperiment'
        elif system is 'lcps':
            sys_use = 'retrieval_system.ListwiseLearningSystemWithCandidateSelection'
            exp_use = 'experiment.HistoricalComparisonExperiment'
        elif system is 'pls':
            sys_use = 'retrieval_system.PrudentListwiseLearningSystem'
            exp_use = 'experiment.PrudentLearningExperiment'

        # make sure to change the spliting here according to the directory of the queries
        # so as to have a correct name for the configuration file
        with open(path_to_c_w+'config_'+q.split('/')[7][:-4]+'_'+system+'.yml', 'wt') as fout:
            # for each line in the template configuration file fill in the
            # parameters
            with open(path_to_c_r+template, 'rt') as fin:
                for line in fin.readlines():
                    fout.write(line.replace('INPUT_FILE', q).
                        replace('OUTPUT_DIR', out_dir).
                        replace('FILE_NAME', q.split('/')[7][:-4]+'_'+system).
                        replace('SYSTEM_USE', sys_use).
                	    replace('EXPERIMENTER_USE', exp_use).
			            replace('NUMRUNS', numruns).
                        replace('NUMFEATURES', numfeatures).
                        replace('NUMQUERIES', numqueries))
