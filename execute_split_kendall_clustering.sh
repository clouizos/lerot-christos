#!/bin/sh

TEMPLATE=$HOME/lerot-christos/lisa/lisa/do_partial_kendallClustering.job
TMP_DIR=$HOME/tmp3

mkdir -p $TMP_DIR

RUNTIME=00
MINUTES=00
RUNDAYS=01
start=`date`
echo $start
DATASET=MSLR



for ROW_MAX in {1000..10000..1000}
do
	let "ROW_MIN=$ROW_MAX-1000"
	for COL_MAX in {1000..10000..1000}
	do
		let "COL_MIN=$COL_MAX-1000"
		JOB_FILE=$TMP_DIR/submit.splitKendallClustering$DATASET'_'$ROW_MAX'_'$COL_MAX.job
		cat $TEMPLATE | sed "s/##RUNTIME##/$RUNTIME/" | \
	          	sed "s/##RUNDAYS##/$RUNDAYS/" | \
	          	sed "s/##MINUTES##/$MINUTES/" | \
	          	sed "s/##TYPEDATA##/$DATASET/" | \
				sed "s/##ROWMAX##/$ROW_MAX/" | \
				sed "s/##ROWMIN##/$ROW_MIN/" | \
				sed "s/##COLMAX##/$COL_MAX/" | \
				sed "s/##COLMIN##/$COL_MIN/" > $JOB_FILE

		echo "Submitting $JOB_FILE"
		qsub -e$JOB_FILE.e -o$JOB_FILE.o $JOB_FILE
		sleep 2
	done
done

