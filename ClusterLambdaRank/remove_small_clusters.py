#!/usr/bin/env python

'''
Utility used to remove small query clusters from the mapping defining the clustering.
'''

import argparse
import numpy as np
import cPickle as pickle


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
    parser.add_argument('-n', type=int, required=True, help='specify the minimum number of queries the cluster has to contain')
    parser.add_argument('input', help='specify the input clustering as pickled mapping QUERY_ID -> CLUSTER_ID')
    parser.add_argument('output', help='specify the output clustering with filtered clusters')

    arguments = parser.parse_args()

    # Load the query clusters.
    with open(arguments.input) as ifile:
        qid2cid = pickle.load(ifile)

    del qid2cid['numofclusters']

    # Count the number of queries in each cluster.
    cluster_sizes = np.bincount(qid2cid.values())

    # Denote the big enough clusters.
    big_enough_cids = list(np.where(cluster_sizes > arguments.n)[0])

    # Define new clusters (remove small ones and renumber the old ones).
    clusters = dict((qid, big_enough_cids.index(cid) + 1) for qid, cid in qid2cid.iteritems() if cid in big_enough_cids)
    clusters['numofclusters'] = len(big_enough_cids)

    with open(arguments.output, 'w') as ofile:
        pickle.dump(clusters, ofile, protocol=-1)
