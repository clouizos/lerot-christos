from __future__ import division
import numpy as np
from itertools import combinations


def pos_weighted_kendalltau(X, Y):
    """
    Function that returns the position weighted Kendall's tau between
    two ranking lists X and Y. Naive implementation of [1]. In dire
    need of optimization since it was implemented quick and dirty.

    Parameters
    -----------
    X : list of ints
        First ranking of document IDs
        (each int in the list is a document ID)

    Y : list of ints
        Second ranking of document IDs

    Output
    -------
    tau : float
        Generalized kendall tau distance between X and Y

    n_inversions : int
        Number of inverted rankings between X and Y

    References
    -----------
    [1] Kumar, Ravi, and Sergei Vassilvitskii. "Generalized distances between rankings."
        Proceedings of the 19th international conference on World wide web. ACM, 2010.
    """
    # both lists to have the same size
    assert len(X) == len(Y) and len(X) > 0
    # find out the largest document ID
    all_docs = max(set(X).union(set(Y)))
    # initialize dummy rankings
    # for all the documents up to the largest document ID
    # len(X) + 1 because for missing documents (document in X and
    # not in Y and document in Y but not in X) we assume that their
    # ranking is one position below the ranking we already have
    ranks_a, ranks_b = [len(X) + 1] * all_docs, [len(X) + 1] * all_docs
    tau, n_inversions = 0.0, 0
    # every document from 0 up to largest document ID
    # is a possible candidate
    n_candidates = all_docs

    # rank_a and rank_b are lists of tuples
    # where each tuple is (document, position in ranking list)
    rank_a = [(x, i+1) for i, x in enumerate(X)]
    # sort them accoring to the document ID
    rank_a.sort(key=lambda x: x[0])
    # similarly for Y
    ########################## NOTE ###################################
    # By sorting them according to document ID we can iterate all the #
    # documents regardless of their rank and measure the agreement    #
    # and disagreement between the two ranking lists by usign the     #
    # second element of the tuple.                                    #
    ###################################################################
    rank_b = [(y, i+1) for i, y in enumerate(Y)]
    rank_b.sort(key=lambda x: x[0])

    # assing to each document its ranking
    # ranks_a ranking from X
    # ranks_b ranking from Y
    for i in rank_a:
        ranks_a[i[0]-1] = i[1]
    for i in rank_b:
        ranks_b[i[0]-1] = i[1]

    # these are the weights mentioned in the paper
    # delta = lambda i: [1./j for j in i]
    # pi = lambda i: sum(delta(range(1, i)))

    # for the position weights delta - DCG like delta
    # found here : http://theory.stanford.edu/~sergei/slides/www10-metrics.pdf
    delta = lambda i: (1/np.log(i)) - (1/(np.log(i) + 1))
    # position weight p_i = sum_{j=2}^i(delta_j)
    pi = lambda i: sum(delta(range(2, i+1)))

    # for each document combination
    for i, j in combinations(xrange(n_candidates), 2):
        # if they agree continue
        if (np.sign(ranks_a[i] - ranks_a[j]) == 0) and (-np.sign(ranks_b[i] - ranks_b[j]) == 0):
            continue
        # if they disagree
        if (np.sign(ranks_a[i] - ranks_a[j]) == -np.sign(ranks_b[i] - ranks_b[j])):
            # increase the number of inversions
            n_inversions += 1

            # add the cost according to the position changed
            # for the first document
            if ranks_a[i] == ranks_b[i]:
                pi_sigma = 1  # on the paper it mentioned something about pi being one
                              # not sure if it makes sense
            else:
                pi_sigma = (pi(ranks_a[i]) - pi(ranks_b[i]))/(ranks_a[i] - ranks_b[i])

            # for the second document
            if ranks_a[j] == ranks_b[j]:
                pj_sigma = 1
            else:
                pj_sigma = (pi(ranks_a[j]) - pi(ranks_b[j]))/(ranks_a[j] - ranks_b[j])

            # increase kendall's tau
            tau += pi_sigma * pj_sigma

    return tau, n_inversions

if __name__ == '__main__':
    # various tests to check if the implementation is correct
    print 'Equality:'
    print pos_weighted_kendalltau([1, 2, 3], [1, 2, 3])
    print 'Low difference:'
    print pos_weighted_kendalltau([1, 2, 3], [1, 3, 2])
    print 'Medium difference:'
    print pos_weighted_kendalltau([1, 2, 3], [2, 1, 3])
    print 'Higher difference:'
    print pos_weighted_kendalltau([1, 2, 3], [2, 3, 1])
    print 'One equal rank:'
    print pos_weighted_kendalltau([1, 2, 3], [3, 2, 1])
    print 'Extra numbers:'
    print pos_weighted_kendalltau([1, 2, 3, 4, 6], [3, 2, 1, 10, 4])
