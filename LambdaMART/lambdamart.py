#!/usr/bin/env python
# -*- coding: utf-8 -*-

import warnings
import numpy as np
import cPickle as pickle

from lerot.utils import rank
from functools import  partial
from bs4 import BeautifulSoup, Tag
from sklearn.tree._tree import Tree, DTYPE, DOUBLE, NODE_DTYPE, TREE_UNDEFINED, TREE_LEAF

try:
    # FIXME: Would be nice to make this work.
    from addnodehack import add_node
except ImportError, ValueError:  
    # ImportError: No module named addnodehack
    # ValueError: sklearn.tree._tree.Splitter has the wrong size, try recompiling

    # Define a dummy add_node method.
    def add_node(*args):
        raise NotImplementedError("This does not work until someone resolves the 'addnodehack compilation' issue!")
    

def tag_filter(elements):
    '''
    Filter the elements (iterable) leaving only Tag elements.
    '''
    return filter(lambda e: isinstance(e, Tag), elements)


def split_filter(elements):
    '''
    Filter the elements (iterable) leaving only <split> XML tag elements.
    '''
    return filter(lambda e: e.name == 'split', tag_filter(elements))


def build_tree(soup_tree, n_features):
    '''
    Build Tree object (sklearn.tree._tree.Tree) from the specified
    XML description in BeautifulSoup parser.

    The parameter 'n_features' is needed to initialize the dimension
    of the internal Tree structure (NumPy arrays).
    '''
    stack = [(np.intp(0), split_filter(soup_tree.children)[0])]

    weight = DOUBLE(soup_tree.weight)

    tree = Tree(n_features, np.array([1], dtype=np.intp), 1)

    while stack:
        parent, split = stack.pop()

        feature = np.intp(split.feature.contents)
        threshold = DOUBLE(split.threshold.contents)

        is_left = (split.attrs.get('pos') == 'left')
        is_leaf = False

        # Pretty nasty way of finding whether this split element
        # is actually a leaf.
        for tag in tag_filter(split.children):
            if tag.name == 'output':
                is_leaf = True
                break

        node_id = add_node(tree, parent, is_left, is_leaf, feature, threshold,
                           DOUBLE(0), np.intp(0), DOUBLE(0))

        if is_leaf:
            tree.value[node_id] = weight * DOUBLE(split.output.contents)

        for split in split_filter(split.children):
            stack.append((node_id, split))


def build_tree_workaround(soup_tree, n_features=-1, init_capacity=128):
    # The weight of a tree, supposedly LambdaMART's learning rate.
    weight = DOUBLE(soup_tree.attrs['weight'])

    # The internal data structures defining sklearn's Ttree.
    n_nodes = 0
    nodes   = np.empty(init_capacity, dtype=NODE_DTYPE)
    values  = np.empty(init_capacity, dtype=DOUBLE)

    nodes.fill(TREE_UNDEFINED)
    values.fill(TREE_UNDEFINED)

    capacity = init_capacity

    max_feature = -1

    # Stack is used instead of recursion.
    stack = [(TREE_UNDEFINED, split_filter(soup_tree.children)[0])]

    while stack:
        parent, split = stack.pop()

        is_leaf = False

        # Pretty nasty way of finding whether this split element
        # is actually a leaf.
        for tag in tag_filter(split.children):
            if tag.name == 'output':
                is_leaf = True
                break

        is_left = (split.attrs.get('pos') == 'left')

        if is_leaf:
            value = weight * DOUBLE(split.output.contents)
        else:
            feature = np.intp(split.feature.contents)
            max_feature = max(max_feature, feature)
            threshold = DOUBLE(split.threshold.contents)

        # The current node identifier.
        node_id = n_nodes

        # The number of nodes exceeded the estimated capacity.
        if node_id >= capacity:
            # Inflate the arrays to account for new nodes.
            nodes = np.resize(nodes, (2 * capacity,))
            values = np.resize(values, (2 * capacity,))
            # We use numpy.resize instead of ndarray.resize here
            # to make sure everything ends up alright.
            nodes[capacity:].fill(TREE_UNDEFINED)
            values[capacity:].fill(DOUBLE(0))
            capacity = 2 * capacity

        # If node is not the root of the tree.
        if parent != TREE_UNDEFINED:
            if is_left:
                nodes[parent]['left_child'] = node_id
            else:
                nodes[parent]['right_child'] = node_id

        if is_leaf:
            nodes[node_id]['left_child'] = TREE_LEAF
            nodes[node_id]['right_child'] = TREE_LEAF
            nodes[node_id]['feature'] = TREE_UNDEFINED
            nodes[node_id]['threshold'] = TREE_UNDEFINED
            values[node_id] = value
        else:
            # FIXME: Feature indexing should be marked somewhere if it
            # realy is 0-based or not. Here we are assuming it is NOT
            # because of RankLib!!!
            nodes[node_id]['feature'] = feature - 1
            nodes[node_id]['threshold'] = threshold

        n_nodes += 1

        for split in split_filter(split.children):
            stack.append((node_id, split))

    # Compactify the arrays to save some (unused) memory.
    nodes = np.resize(nodes, (n_nodes,))
    values = np.resize(values, (n_nodes,))

    # Reshape the values array to meet the sklearn's Tree expectations.
    values = values.reshape((values.shape[0], 1, 1))

    # Warn in case the maximum feature was bigger then specified number of features.
    if n_features > 0 and max_feature >= n_features:
        warnings.warn('n_features (%d) was too small, max. feature index found: %d' % (n_features, max_feature))

    # Exploit the methods for pickling... aaargh!
    tree = Tree(n_features, np.array([1], dtype=np.intp), 1)
    tree.__setstate__(dict(node_count=n_nodes, nodes=nodes, values=values))

    return tree


def write_tree(tree, id, output, padding='\t'):
    '''
    Write the specified tree (sklearn.tree._tree.Tree) into the output
    in a text format similar to RankLib.
    '''
    output.write(padding + '<tree id="%d">\n' % id)

    # Stack of 3-tuples: (depth, parent, node_id).
    stack = [(1, TREE_UNDEFINED, 0)]

    while stack:
        depth, parent, node_id = stack.pop()

        # End of split mark.
        if node_id < 0:
            output.write(padding + (depth * '\t'))
            output.write('</split>\n')
            continue

        output.write(padding + (depth * '\t'))
        output.write('<split')

        if parent == TREE_UNDEFINED:
            output.write('>\n')
        else:
            pos = 'left' if tree.children_left[parent] == node_id else 'right'
            output.write(' pos="%s">\n' % pos)

        # If the node is a leaf.
        if tree.children_left[node_id] == TREE_LEAF:
            output.write(padding + ((depth + 1) * '\t'))
            output.write('<output> %.16f </output>\n' % tree.value[node_id])
            output.write(padding + (depth * '\t'))
            output.write('</split>\n')
        else:
            output.write(padding + ((depth + 1) * '\t'))
            # FIXME: Feature indexing should be marked somewhere if it
            # realy is 0-based or not. Here we are assuming it is NOT
            # because of RankLib!!!
            output.write('<feature> %d </feature>\n' % (tree.feature[node_id] + 1))
            output.write(padding + ((depth + 1) * '\t'))
            output.write('<threshold> %.8f </threshold>\n' % tree.threshold[node_id])

            # Push the end of split mark first... then push the right and left child.
            stack.append((depth, parent, -1))
            stack.append((depth + 1, node_id, tree.children_right[node_id]))
            stack.append((depth + 1, node_id, tree.children_left[node_id]))

    output.write(padding + '</tree>\n')


class LambdaMART(object):
    '''
    LambdaMART learning to rank model.
    '''
    def __init__(self, trees):
        if not isinstance(trees, list):
            if not isinstance(trees, Tree):
                raise ValueError('trees must be a list of sklearn.tree._tree.Tree(s)')
            else:
                trees = [trees]    
        self.trees = trees

    @staticmethod
    def load_from_text(filepath, n_features=-1):
        with open(filepath) as ifile:
            return LambdaMART(map(partial(build_tree_workaround, n_features=n_features),
                                  BeautifulSoup(ifile).find_all('tree')))

    def save_to_text(self, filepath):
        from StringIO import StringIO
        buffer = StringIO()

        buffer.write('<ensemble>\n')
        for id, tree in enumerate(self.trees, start=1):
            write_tree(tree, id, buffer)
        buffer.write('</ensemble>\n')

        with open(filepath, 'w') as ofile:
            ofile.write(buffer.getvalue())
        buffer.close()

    @staticmethod
    def load(filepath):
        with open(filepath) as ifile:
            return pickle.load(ifile)

    def save(filepath):
        with open(filepath, 'wb') as ofile:
            pickle.dump(self, filepath)

    def predict(self, X):
        X = np.asanyarray(X, dtype=DTYPE)

        scores = np.zeros((X.shape[0], 1), dtype=DOUBLE)

        for tree in self.trees:
            scores += tree.predict(X)

        return scores

    def init_ranking(self, query, ties='first'):
        self.qid = query.get_qid()

        scores = self.predict(query.get_feature_vectors())

        pos2rank = rank(scores, ties=ties, reverse=False)

        ranked_docids = []
        for pos, docid in enumerate(query.__docids__):
            ranked_docids.append((pos2rank[pos], docid))

        ranked_docids.sort(reverse=True)

        self.docscores = scores.ravel()
        self.docids = [docid for (_, docid) in ranked_docids]

    def get_ranking(self):
        return self.docids

    def get_scores(self):
        return list(self.docscores)
