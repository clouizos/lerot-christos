#!/bin/bash

DataDir=config_pq_100/
Files=($DataDir*.yml)
nFiles=${#Files[@]}

START=`date`
echo $START
step=6

for i in `seq 0 $step $nFiles`
do
     #echo i:$i
     for j in `seq $(($i)) 1 $(($i + $step - 1))`
     do
	  if [ $j -ge $nFiles ]
          then
               break
          fi
          #echo $j:${Files[j]}
          COMMAND="python scripts/learning-experiment.py -f ${Files[j]} &"
          echo "Running $COMMAND"
          eval $COMMAND
          sleep 2
     done
     wait
done
