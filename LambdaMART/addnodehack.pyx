cimport numpy as np

ctypedef np.npy_intp SIZE_t

from sklearn.tree._tree cimport Tree

cdef SIZE_t add_node(Tree tree, SIZE_t parent, bint is_left, bint is_leaf,
                     SIZE_t feature, double threshold, double impurity,
                     SIZE_t n_node_samples, double weighted_n_node_samples):
    return tree._add_node(parent, is_left, is_leaf, feature, threshold, impurity, n_node_samples, weighted_n_node_samples)
