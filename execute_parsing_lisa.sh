#!/bin/sh

TEMPLATE=$HOME/lerot-christos/lisa/lisa/parse_rankers.template.job
TMP_DIR=$HOME/tmp3

mkdir -p $TMP_DIR

RUNTIME=00
MINUTES=00
RUNDAYS=02

DataDir=$HOME/Intent/Data/$1/Config_pq_all
Groups=($DataDir/Group*/)
nGroups=${#Groups[@]}
START=`date`
echo $START
step=1

for i in `seq 0 $step $nGroups`
do
     IFS='/' read -ra comp <<< "${Groups[i]}"
     #comp[7]=Group1
     JOB_FILE=$TMP_DIR/submit.${comp[7]}.job
     cat $TEMPLATE | sed "s/##RUNTIME##/$RUNTIME/" | \
          sed "s/##RUNDAYS##/$RUNDAYS/" | \
          sed "s/##MINUTES##/$MINUTES/" | \
          sed "s/##GROUPQ##/${comp[7]}/" | \
          sed "s/##WHICHDATA##/$1/" > $JOB_FILE

     echo "Submitting $JOB_FILE"
     qsub -e$JOB_FILE.e -o$JOB_FILE.o $JOB_FILE
     sleep 2
     #break
done
