#!/bin/sh

TEMPLATE=$HOME/lerot-christos/lisa/lisa/execute_perCluster.job
TMP_DIR=$HOME

RUNTIME=10
MINUTES=00
RUNDAYS=00
start=`date`
echo $start

JOB_FILE=$TMP_DIR/submit.PerClusterTraining$1.job
cat $TEMPLATE | sed "s/##RUNTIME##/$RUNTIME/" | \
          sed "s/##RUNDAYS##/$RUNDAYS/" | \
          sed "s/##MINUTES##/$MINUTES/" | \
	  sed "s/##NUMCL##/$1/" > $JOB_FILE

echo "Submitting $JOB_FILE"
qsub -e$JOB_FILE.e -o$JOB_FILE.o $JOB_FILE
sleep 2

