from __future__ import division
import gzip
import glob
import yaml
from scipy.spatial.distance import pdist, squareform
import numpy as np
from lerot.ranker.ProbabilisticRankingFunction import ProbabilisticRankingFunction
from lerot.comparison.ProbabilisticInterleave import ProbabilisticInterleave
from lerot.environment.CascadeUserModel import CascadeUserModel
from itertools import combinations
from lerot import query
import random
from natsort import natsorted
import cPickle as pickle
#import matplotlib
# if on server use Agg backend, so as to be able
# to save figures even though no gtk available
# try:
#     matplotlib.use('gtk')
# except:
#     matplotlib.use('Agg')
#matplotlib.use('Agg')
#import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import linkage, dendrogram
from sklearn.mixture import DPGMM
#import pylab
import os
#import matplotlib.cm as cm
from sklearn.metrics import silhouette_score


def rankers_for_q(all_rankers, q_id):
    return [q for q in all_rankers if '_'+q_id+'_' in q]


def parse_rankers(ranker_dir, tot_rankers=5, one_query=True, seed=None, already_parsed=[]):
    if seed is not None:
        random.seed(seed)

    rankers = []
    i = 1
    all_rankers = glob.glob(ranker_dir+'train_*.gz')
    if one_query:
        # dummy way to not parse duplicate queries
        while True:
            query_to_check = random.sample(all_rankers, 1)
            q_id = query_to_check[0].split('/')[1].split('_')[2]
            if q_id not in already_parsed:
                break
        print 'Parsing query', q_id
        already_parsed.append(q_id)
        all_rankers = natsorted(rankers_for_q(all_rankers, q_id))[::-1]

    for j, ranker_path in enumerate(all_rankers):
        print 'Parsing', j + 1, 'out of', len(all_rankers)
        ranker = {}
        ranker['path'] = ranker_path
        f = gzip.open(ranker_path, 'rb')
        yml_file = yaml.load(f.read())
        f.close()
        ranker['weights'] = np.asarray(yml_file['final_weights'])
        ranker['weights'] = ranker['weights']/np.linalg.norm(ranker['weights'])
        ranker_args = ranker_path.split(ranker_dir)[1].split('_')
        ranker['query'] = ranker_args[2]
        ranker['system'], ranker['run'] = ranker_args[3].split('.')[0].split('-')
        rankers.append(ranker)
        if not one_query:
            if i == tot_rankers:
                break
            else:
                i += 1
    if one_query:
        return rankers, 'rankers_'+q_id+'.pickle', already_parsed
    else:
        return rankers


# simple similarity measures for a set of rankers from a specific query
def measure_similarity(rankers):
    weight_matrix = []
    similarity_per_metric = {}
    distances = ['euclidean', 'cosine', 'correlation', 'L1']
    for ranker in rankers:
        weight_matrix.append(ranker['weights'])
    weight_matrix = np.asarray(weight_matrix)

    for dist in distances:
        if dist is 'L1':
            sim = pdist(weight_matrix, 'minkowski', 1)
        else:
            sim = pdist(weight_matrix, dist)
        similarity_per_metric[dist] = squareform(sim)

    return similarity_per_metric


# construct the preference matrix over rankers for a given query
def do_pairwise_comparisons(rankers, query_path, num_docs=10, N=1000):
    n_feat = len(rankers[0]['weights'])
    queries = query.load_queries('train_per_query/'+query_path, n_feat)

    ranking_functions = []
    ranker_names = []
    for ranker in rankers:
	x = str(ranker['weights'].tolist()).strip('[]')
        ranking_functions.append(ProbabilisticRankingFunction('3', 'random', \
            n_feat, ','.join(x.split('  ')), sample='sample_unit_sphere'))
        ranker_names.append('ranker_'+ranker['run'])

    print 'Comparing:', ranker_names

    pairs = [pair for pair in combinations(range(len(ranker_names)), 2)]
    pi = ProbabilisticInterleave()

    user_model = CascadeUserModel('--p_click 0:0.05,1:0.3,2:0.5,3:0.7,4:0.95 --p_stop 0:0.2,1:0.3,2:0.5,3:0.7,4:0.9')

    q = queries[queries.keys()[0]]

    preference_matrix = np.zeros((len(ranker_names), len(ranker_names)))

    for i in xrange(N):
        print 'Round:', i+1, 'out of', N
        for pair in pairs:
            (interleaved_list, assignments) = pi.interleave(ranking_functions[pair[0]],
                                         ranking_functions[pair[1]], q, num_docs)
            c = user_model.get_clicks(interleaved_list, q.get_labels())
            outcome = pi.infer_outcome(interleaved_list, assignments, c, q)
            if outcome < 0:  # c1 > c2
                preference_matrix[pair[0], pair[1]] += 1
            elif outcome > 0:  # c2 > c1
                preference_matrix[pair[1], pair[0]] += 1
            else:  # tie
                preference_matrix[pair[0], pair[1]] += 0.5
                preference_matrix[pair[1], pair[0]] += 0.5

    preference_matrix /= N
    preference_matrix[range(preference_matrix.shape[0]),
                      range(preference_matrix.shape[1])] = 0.5
    print 'Preference matrix'
    print preference_matrix
    return preference_matrix


# visualize the mean ranker for a query as well as the std per feature
# maybe some features are more consistent throughout the runs
def ranker_boxplot(rankers):
    weight_matrix = []
    for ranker in rankers:
        weight_matrix.append(ranker['weights'])
    weight_matrix = np.asarray(weight_matrix)

    mean_ranker = np.mean(weight_matrix, axis=0)
    std_ranker = np.std(weight_matrix, axis=0)
    print std_ranker
    plt.plot(range(mean_ranker.shape[0]), mean_ranker, 'o')
    plt.errorbar(range(mean_ranker.shape[0]), mean_ranker, fmt=None, yerr=std_ranker)
    plt.show()


# do a hierarchical clustering of the rankers for a query
# and output a clear visualization of the possible structure
def cluster_rankers(weight_matrix):
    # weight_matrix = []
    # for ranker in rankers:
    #     weight_matrix.append(ranker['weights'])
    # weight_matrix = np.asarray(weight_matrix)

    fig = pylab.figure(figsize=(10, 10))
    sc = pdist(weight_matrix, 'euclidean')  # 'minkowski', 1)
    mat = squareform(sc)

    ax1 = fig.add_axes([0.09, 0.1, 0.2, 0.6])
    Y = linkage(mat, method='single')
    Z = dendrogram(Y, orientation='right')
    ax1.set_xticks([])
    ax1.set_yticks([])

    ax2 = fig.add_axes([0.3, 0.71, 0.6, 0.2])
    Y2 = linkage(mat, method='single')
    Z2 = dendrogram(Y2, orientation='top')
    ax2.set_xticks([])
    ax2.set_yticks([])

    idx1 = Z['leaves']
    idx2 = Z2['leaves']
    mat = mat[idx1, :]
    mat = mat[:, idx2]
    axmatrix = fig.add_axes([0.3, 0.1, 0.6, 0.6])
    im = axmatrix.matshow(mat, origin='lower', aspect='auto', cmap=cm.Blues_r)
    axmatrix.set_xticks([])
    axmatrix.set_yticks([])

    axcolor = fig.add_axes([0.91, 0.1, 0.02, 0.6])
    pylab.colorbar(im, cax=axcolor)
    fig.show()
    fig.savefig('test.jpg')
    plt.show()


def load_rankers(parsed, load_p_group=False, path_g=None):
    q_rankers = []
    if not load_p_group:
        for elem in parsed:
            with open('rankers_pq/rankers_'+elem+'.pickle') as handle:
                q_rankers.append(pickle.load(handle))
    else:
        groups = glob.glob(path_g+'/G*')
        for group in groups:
	    print 'Loading', group.split('/')[-1], '...'
            g_rankers = glob.glob(group+'/ra*')
            for ranker_path in g_rankers:
                with open(ranker_path, 'rb') as handle:
                    q_rankers.append(pickle.load(handle))
            print 'Finished.'

    #print len(q_rankers)
    weight_pqr = []
    for q in q_rankers:
        rankers = []
        for ranker in q:
            #print ranker['weights']
            rankers.append(ranker['weights'])
        weight_pqr.append(rankers)

    #print len(weight_pqr)
    #print weight_pqr
    #print len(weight_pqr[0])
    #tmp = np.zeros((1, 136))
    #for elem in weight_pqr:
    #    elem.append(tmp)
    #    tmp = np.vstack(elem)
   # 	print tmp.shape
    #tmp = np.array(weight_pqr)
    #tmp = tmp[1:, :]
    tot = 0
    #for group in weight_pqr:
    #    for elem in group:
    #        tot += 1
    #print tot
    tmp = np.zeros([(len(weight_pqr)-1)*len(weight_pqr[0]), 136])
    i = 0
    for q in weight_pqr:
        for ranker in q:
            tmp[i, :] = ranker
            i += 1

    #print tmp.shape
    return tmp
    #print tmp[-1, :]
    #print tmp[0]
    #[nq, nr, feat] = tmp.shape
    #return tmp.reshape([nq*nr, feat])


def test_parse():
    ranker_dir = 'Output_q_all/'
    groups = glob.glob(ranker_dir+'G*')
    for i, group in enumerate(groups):
        group_id = group.split('/')[-1]
        print 'Saving for group', i, ',', group, ', out of', len(groups)
        try:
            os.makedirs('rankers_pq_all/'+group_id)
        except:
            print 'Folder already created'
            pass

        num_q = set([elem.split('/')[-1].split('_')[-2] for elem in glob.glob(ranker_dir+group_id+'/t*gz')])
        print num_q
        print len(num_q)
        already_parsed = [q.split('/')[1].split('_')[1].strip('.pickle') for q in glob.glob('rankers_pq_all/'+group_id+'/*pickle')]
        how_much = len(num_q) - len(already_parsed)
        for i in xrange(how_much):
            print 'Query', i, 'out of', how_much
            rankers, path_save, already_parsed = parse_rankers(ranker_dir+group_id+'/', tot_rankers=20, one_query=True, seed=None, already_parsed=already_parsed)
            print 'Saving rankers...'
            with open('rankers_pq_all/'+group_id+'/'+path_save, 'wb') as handle:
                pickle.dump(rankers, handle)
            print 'Saved.'
            print already_parsed


def test_cluster():
    from sklearn.cluster import MiniBatchKMeans
    print 'Loading rankers...'
    rankers = load_rankers('', load_p_group=True, path_g='rankers_pq_all')
    print 'Loaded. ', rankers.shape
    n_sil_sample = 15

    number_clusters = range(2, 51)
    batch_size = 500
    acc_inertia = []
    acc_sil = []
    for n_cl in number_clusters:
        print 'Attempting', n_cl, 'clusters...'
        mbk = MiniBatchKMeans(k=n_cl, init='k-means++', batch_size=batch_size,
                      n_init=10, max_no_improvement=10, verbose=0)
        mbk.fit(rankers)
        #sils = []
        #for i in xrange(n_sil_sample):
        #    print 'Sampling for sil', i + 1, 'out of', n_sil_sample
        #    sils.append(silhouette_score(rankers, mbk.labels_, sample_size=5000, metric='euclidean'))
        #sil = np.mean(sils)
        print 'Inertia with', n_cl, 'clusters:', mbk.inertia_
        #print 'Silhouette with', n_cl, 'clusters:', sil
        acc_inertia.append(mbk.inertia_)
        #acc_sil.append(sil)

    print
    print acc_inertia
    #print acc_sil
    print 'Best number of clusters according to inertia was', number_clusters[np.asarray(acc_inertia).argmin()]
    #print 'Best number of clusters according to silhouette was', number_clusters[np.asarray(acc_sil).argmax()]


if __name__ == '__main__':
    # test_parse()
    test_cluster()
