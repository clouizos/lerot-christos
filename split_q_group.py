import os
import shutil
import glob
import sys

"""Script that splits the generate lerot config files into Groups allowing
    for distributed estimation of the rankers for each query.
    Takes as an argument the dataset that you want to work on. ('MSLR', 'YLR')"""

# parse all the generated config files
directory = '../Intent/Data/'+sys.argv[1]+'/'
config_files = glob.glob(directory+'Config_pq_all/*.yml')
# lambda expression for splitting the list into chunks
lof = lambda lst, sz: [lst[i:i+sz] for i in range(0, len(lst), sz)]
# do the split into the number of groups that was specified
split_f = lof(config_files, int(sys.argv[2]))

# for each group
for i, gr in enumerate(split_f):
    # make the directory if it doesnt exist
    os.makedirs(directory+'Config_pq_all/Group'+str(i))
    for elem in gr:
        # move each config for a query to its respective group
        shutil.move(elem, directory+'Config_pq_all/Group'+str(i))
