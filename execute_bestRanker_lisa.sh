#!/bin/sh

TEMPLATE=$HOME/lerot-christos/lisa/lisa/find_bestRanker.template.job
TMP_DIR=$HOME/tmp4

mkdir -p $TMP_DIR

RUNTIME=00
MINUTES=00
RUNDAYS=02

DataDir=$HOME/lerot-christos/rankers_pq_all_$1
Groups=($DataDir/Group*/)
nGroups=${#Groups[@]}
START=`date`
echo $START
step=1

for i in `seq 0 $step $nGroups`
do
     IFS='/' read -ra comp <<< "${Groups[i]}"
     JOB_FILE=$TMP_DIR/submit.${comp[5]}.job
     cat $TEMPLATE | sed "s/##RUNTIME##/$RUNTIME/" | \
          sed "s/##RUNDAYS##/$RUNDAYS/" | \
          sed "s/##MINUTES##/$MINUTES/" | \
          sed "s/##GROUPQ##/${comp[5]}/" | \
          sed "s/##TYPEDATA##/$1/" > $JOB_FILE

     echo "Submitting $JOB_FILE"
     qsub -e$JOB_FILE.e -o$JOB_FILE.o $JOB_FILE
     sleep 2
     #break
done
