#!/bin/bash

RankLibDIR='../ClusterLambdaRank'

# Check that RankLib LambdaMART models has been trained
if [ ! -d "${RankLibDIR}" -o  ! "$(ls -A ${RankLibDIR}/models)" ]
then
    echo "You need to train the LambdaMART models first. Go to ${RankLibDIR} and run 'run_cluster_lambdamart.sh'!"
    exit 1
fi

echo -n "Preparing the datasets..."
# Prepare the cluster training and test datasets in pickles (altough we need only test sets).
./prepare_clustered_dataset_pickles.py --seed 42 ${RankLibDIR}/clusters/sampled_queries_MSLR.pkl ${RankLibDIR}/clusters/qclusters_MSLR_wo1.pkl datasets
echo " done."

echo "Saving the scores and models to './test_scores' and './test_models', respectively, for you inspection."
./test_lambdamart.py --scores test_score --output-models test_models datasets ${RankLibDIR}/models evaluation_report.txt
echo "You will find the evaluation of Python models (loaded from RankLib LambdaMART models) in './evalution_report.txt'"
