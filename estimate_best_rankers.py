import glob
import numpy as np
import os
import cPickle as pickle
import sys
from multiprocessing import Pool
from lerot.ranker.ProbabilisticRankingFunction import ProbabilisticRankingFunction
from lerot.comparison.ProbabilisticInterleave import ProbabilisticInterleave
from lerot.environment.CascadeUserModel import CascadeUserModel
from lerot import query
from itertools import combinations


def do_pairwise_comparisons(rankers, query_path, folder_save, num_docs=10, N=1000):
    """
    Function that creates a preference matrix over rankers for a given query via
    pairwise interleaved comparisons.

    Parameters
    -----------
    rankers : list of dictionaries
        Each dictionary corresponds to a ranker. The keys of this dictionary are:
            'query': The query of the ranker
            'run': On which 'run' the ranker was created (we created a ranker for a query by picking the best one
                    among a set of trained rankers, each one of them obtained from a separate 'run')
            'system': The system that was used to train the ranker (e.g. Listwise Learning System - 'lws')
            'weights': A numpy array that contains the weights of the ranker

    query_path : string
        The location of the specific query dataset (query-document feature vectors).

    folder_save : string
        The folder where the best ranker for this query will be saved.

    num_docs : int, optional
        The number of top-K documents that will be used for interleaving comparisons
        between the rankers. Defaults to 10.

    N : int, optional
        The number of iterations for the online evaluation. Defaults to 1000.

    Output
    -------
    preference_matrix : numpy array
        A matrix of size [len(rankers), len(rankers)] that contains the win rates
        between the rankers

    best_ranker : dictionary
        Dictionary corresponding to the best ranker.

    The function will also save in folder_save the pickle for the best ranker with
    the name: best_ranker_'+best_ranker['query']+'.pkl'
    """
    n_feat = len(rankers[0]['weights'])
    queries = query.load_queries(query_path, n_feat)

    ranking_functions = []
    ranker_names = []
    # gather the rankers and create the ProbabilisticRankingFunction instances
    for ranker in rankers:
        x = str(ranker['weights'].tolist()).strip('[]')
        ranking_functions.append(ProbabilisticRankingFunction('3', 'random',
            n_feat, ','.join(x.split('  ')), sample='sample_unit_sphere'))
        # save also the names of the rankers
        ranker_names.append('ranker_'+ranker['run'])

    print 'Comparing:', ranker_names

    # create all possible pairwise combinations between the rankers
    pairs = [pair for pair in combinations(range(len(ranker_names)), 2)]
    # create the interleaving object
    pi = ProbabilisticInterleave()
    # create the user model
    user_model = CascadeUserModel('--p_click 0:0.05,1:0.3,2:0.5,3:0.7,4:0.95 --p_stop 0:0.2,1:0.3,2:0.5,3:0.7,4:0.9')
    # get the query that will be used for the experiments
    # (only 1 query available at the queries object)
    q = queries[queries.keys()[0]]
    # empty preference matrix
    preference_matrix = np.zeros((len(ranker_names), len(ranker_names)))
    # for each round
    for i in xrange(N):
        print 'Round:', i+1, 'out of', N
        # for each pair
        for pair in pairs:
            # interleave
            (interleaved_list, assignments) = pi.interleave(ranking_functions[pair[0]],
                                         ranking_functions[pair[1]], q, num_docs)
            # get clicks
            c = user_model.get_clicks(interleaved_list, q.get_labels())
            # find outcome and assign credit
            outcome = pi.infer_outcome(interleaved_list, assignments, c, q)
            if outcome < 0:  # c1 > c2
                preference_matrix[pair[0], pair[1]] += 1
            elif outcome > 0:  # c2 > c1
                preference_matrix[pair[1], pair[0]] += 1
            else:  # tie
                preference_matrix[pair[0], pair[1]] += 0.5
                preference_matrix[pair[1], pair[0]] += 0.5

    # normalize the matrix
    preference_matrix /= N
    # .5 probability for win rate between a pair of the same rankers
    preference_matrix[range(preference_matrix.shape[0]),
                      range(preference_matrix.shape[1])] = 0.5
    print 'Preference matrix'
    print preference_matrix
    # return preference_matrix

    # determine condorcet winner
    det_condorcet = np.zeros(len(ranker_names))
    for i in xrange(preference_matrix.shape[0]):
        det_condorcet[i] = len(preference_matrix[i, :][preference_matrix[i, :] > 0.5])

    best_id = det_condorcet.argmax()

    print 'Best ranker:', ranker_names[best_id]
    best_ranker = rankers[best_id]
    with open(folder_save+'/best_ranker_'+best_ranker['query']+'.pkl', 'wb') as handle:
        pickle.dump(best_ranker, handle)

    return preference_matrix, best_ranker


def helper_estimate(input_p):
    """Helper function for doing parallel estimation
        of the best ranker for a given query
        (Basically unroll the arguments and call
        the do_pairwise_comparisons function accordingly)"""
    ranker_path, folder_save, typedata = input_p
    with open(ranker_path, 'rb') as handle:
        rankers_q = pickle.load(handle)
    if typedata != 'YLR':
        query_path = '/home/mzoghi/Intent/Data/MSLR/train_per_query/train_query_'+rankers_q[0]['query']+'.txt'
    else:
        query_path = '/home/mzoghi/Intent/Data/YLR/train_per_query/query1_'+rankers_q[0]['query']+'.txt'
    do_pairwise_comparisons(rankers_q, query_path, folder_save, num_docs=10, N=1000)


if __name__ == '__main__':
    # get the corresponding group of rankers
    ranker_group = sys.argv[1]  # /home/mzoghi/lerot-christos/rankers_pq_all/Group0/
    # where to save the best rankers
    ranker_out_dir = 'BestRankers'
    # infer the dataset from the path to the directory, CAUTION if you have different path
    typedata = ranker_group.split('/')[-3].split('_')[-1]
    print ranker_group.split('/')
    print typedata
    print 'Doing', ranker_group.split('/')[-1]

    # create the save directory if it doesnt exist
    try:
        os.makedirs(ranker_group+ranker_out_dir)
    except:
        print 'Folder already created'
        pass

    # print 'Splitting queries into parts...'
    # queries = list(set([elem.split('/')[-1].split('_')[-2] for elem in glob.glob(ranker_dir+'/t*gz')]))

    # generate the function arguments
    all_q_group = glob.glob(ranker_group+'rank*')
    params = zip(all_q_group, [ranker_group+ranker_out_dir] * len(all_q_group), [typedata] * len(all_q_group))
    print params

    # and do the parallel processing
    print 'Initializing pool...'
    pool = Pool()
    print 'Running...'
    pool.map(helper_estimate, params)
    pool.close()
    pool.join()
    print 'Done...'
